-- MySQL dump 10.13  Distrib 8.0.33, for macos13.3 (arm64)
--
-- Host: localhost    Database: zmanager
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE="+00:00" */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE="NO_AUTO_VALUE_ON_ZERO" */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `zeta_dokumen`
--

DROP TABLE IF EXISTS `zeta_dokumen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zeta_dokumen` (
  `id_dokumen` varchar(36) NOT NULL,
  `id_jns_dokumen` varchar(36) DEFAULT NULL,
  `file_name` varchar(100) DEFAULT NULL,
  `deskripsi` varchar(100) DEFAULT NULL,
  `file_path` varchar(100) DEFAULT NULL,
  `file_size` decimal(7,2) DEFAULT NULL,
  `file_type` varchar(100) DEFAULT NULL,
  `model` varchar(100) DEFAULT NULL,
  `id_model` varchar(36) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_dokumen`),
  KEY `dokumen_FK` (`id_jns_dokumen`),
  CONSTRAINT `dokumen_FK` FOREIGN KEY (`id_jns_dokumen`) REFERENCES `zeta_ref_jns_dokumen` (`id_jns_dokumen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zeta_dokumen`
--

LOCK TABLES `zeta_dokumen` WRITE;
/*!40000 ALTER TABLE `zeta_dokumen` DISABLE KEYS */;
INSERT INTO `zeta_dokumen` VALUES ("0021509b-a429-4872-9234-0484018f6f54","133174b3-eec3-42fd-b0b6-1286e086f079","3820430584_Screenshot_2023-07-14_at_17.15.38.png",NULL,"2023/07/15/",47.90,"image/png","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2023-07-15 15:14:33","2023-07-15 15:16:12","2023-07-15 15:16:12"),("1df03afc-408c-41d8-8361-fd08dcbf65b0","133174b3-eec3-42fd-b0b6-1286e086f079","2210576763_Screenshot from 2021-09-08 08-03-40.png",NULL,"2021/09/22/",821.08,"image/png","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2021-09-22 02:04:01","2021-09-22 02:06:38","2021-09-22 02:06:38"),("59df2ef1-8dac-48bf-819f-ab35fcf2da01","133174b3-eec3-42fd-b0b6-1286e086f079","2677751685_sae.jpeg",NULL,"2021/09/22/",172.31,"image/jpeg","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2021-09-22 02:07:51","2021-09-22 02:08:35","2021-09-22 02:08:35"),("62ffd99c-775a-4a14-9181-ca3366a5c880","133174b3-eec3-42fd-b0b6-1286e086f079","3623211161_sae.jpeg",NULL,"2021/09/22/",172.31,"image/jpeg","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2021-09-22 02:06:38","2021-09-22 02:07:51","2021-09-22 02:07:51"),("6d186df6-6bd8-4580-b073-5c72e85ea8f2","133174b3-eec3-42fd-b0b6-1286e086f079","3002948710_Screenshot from 2021-09-13 08-26-02.png",NULL,"2021/09/22/",776.20,"image/png","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2021-09-22 02:03:49","2021-09-22 02:04:01","2021-09-22 02:04:01"),("72a85460-7051-4e73-a519-f91face91b77","133174b3-eec3-42fd-b0b6-1286e086f079","1851805415_Screenshot_2023-07-07_at_14.46.10.png",NULL,"2023/07/15/",537.20,"image/png","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2023-07-15 15:17:02","2023-07-23 08:05:53","2023-07-23 08:05:53"),("80bab6ff-f40c-49e3-8d31-eb25cabe2fc4","133174b3-eec3-42fd-b0b6-1286e086f079","3451807358_Screenshot_2023-06-21_at_10.54.40.png",NULL,"2023/07/23/",159.28,"image/png","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2023-07-23 08:05:53","2023-07-23 08:10:53","2023-07-23 08:10:53"),("870cee4c-e97e-4e06-ae91-4632fb004d09","133174b3-eec3-42fd-b0b6-1286e086f079","2616111282_Screenshot from 2021-09-17 09-22-53.png",NULL,"2021/09/22/",248.45,"image/png","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2021-09-22 02:02:38","2021-09-22 02:03:49","2021-09-22 02:03:49"),("b4097dd5-4e96-4c85-beba-7b124985db26","133174b3-eec3-42fd-b0b6-1286e086f079","1800385745_sae.jpeg",NULL,"2021/09/22/",172.31,"image/jpeg","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2021-09-22 02:08:35","2021-09-22 02:09:22","2021-09-22 02:09:22"),("b9703c32-f1c9-4f17-9c68-4da561070fce","133174b3-eec3-42fd-b0b6-1286e086f079","2072091299_Screenshot from 2021-09-17 11-40-02.png",NULL,"2021/09/22/",106.39,"image/png","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2021-09-22 02:01:03","2021-09-22 02:02:38","2021-09-22 02:02:38"),("ba9dabb0-35d8-4b32-9686-0ddf46ad0b6a","133174b3-eec3-42fd-b0b6-1286e086f079","2902300950_Screenshot_2023-07-14_at_17.15.38.png",NULL,"2023/07/15/",47.90,"image/png","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2023-07-15 15:16:12","2023-07-15 15:17:02","2023-07-15 15:17:02"),("bd862d95-8079-4de4-a51b-3a0ece4716a1","133174b3-eec3-42fd-b0b6-1286e086f079","3062346862_sae_byeok.jpeg",NULL,"2021/09/22/",172.31,"image/jpeg","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2021-09-22 02:09:22","2023-07-15 15:14:33","2023-07-15 15:14:33"),("c8cbcf53-ac78-446e-9fc6-414b57f3f4fe","133174b3-eec3-42fd-b0b6-1286e086f079","2185964379_Screenshot from 2021-09-08 14-57-42.png",NULL,"2021/09/22/",192.60,"image/png","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2021-09-22 00:56:38","2021-09-22 00:56:38","2021-09-22 00:56:38"),("c955a679-19f4-472d-bf13-6b86e124568d","133174b3-eec3-42fd-b0b6-1286e086f079","3781841216_Screenshot_2023-07-19_at_12.27.49.png",NULL,"2023/07/23/",1026.90,"image/png","App\\Modules\\Users\\Models\\Users","3069bd28-7104-42e3-957f-65e8534e2fd7","2023-07-23 08:10:53","2023-07-23 08:10:53",NULL);
/*!40000 ALTER TABLE `zeta_dokumen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zeta_menu_grup`
--

DROP TABLE IF EXISTS `zeta_menu_grup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zeta_menu_grup` (
  `id_menu_grup` varchar(36) NOT NULL,
  `nm_menu_grup` varchar(100) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `urutan` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu_grup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zeta_menu_grup`
--

LOCK TABLES `zeta_menu_grup` WRITE;
/*!40000 ALTER TABLE `zeta_menu_grup` DISABLE KEYS */;
INSERT INTO `zeta_menu_grup` VALUES ("0ee195cf-24b1-4a57-83fb-4addaebec367","Menu Utama","fa fa-folder",1,"2021-08-26 20:56:14","2023-07-22 09:16:55",NULL),("63ea1746-5930-4846-92ce-816a1e277b4d","swdw","fa fa-folder",1,"2023-07-23 08:44:35","2023-07-23 08:44:41","2023-07-23 08:44:41"),("83258632-3d71-4478-aa64-b1bf62345770","Pengaturan","fa fa-folder",3,"2021-09-10 07:32:48","2021-09-10 07:32:48",NULL),("964988a5-a287-42f2-b61b-1caa3055657e","Data Utama","fa fa-folder",2,"2021-09-01 00:03:45","2021-09-01 00:03:45",NULL),("c4a23c52-d315-4c3a-b2e7-988a19293713","tes abc","fa",2,"2023-07-15 15:51:24","2023-07-15 15:51:36","2023-07-15 15:51:36"),("d971436c-54ec-4216-833d-d84645dc0ac1","wd","dwd",1,"2023-07-15 15:50:31","2023-07-15 15:51:32","2023-07-15 15:51:32"),("fe73d568-a426-4753-aa6a-fccec90a6ce0","Data Referensi","fa fa-folder",4,"2021-09-01 00:03:31","2021-09-11 18:35:15",NULL);
/*!40000 ALTER TABLE `zeta_menu_grup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zeta_migrations`
--

DROP TABLE IF EXISTS `zeta_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zeta_migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zeta_migrations`
--

LOCK TABLES `zeta_migrations` WRITE;
/*!40000 ALTER TABLE `zeta_migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `zeta_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zeta_module`
--

DROP TABLE IF EXISTS `zeta_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zeta_module` (
  `id_module` varchar(36) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `slug` varchar(50) DEFAULT NULL,
  `is_show` tinyint DEFAULT NULL,
  `id_menu_grup` varchar(36) DEFAULT NULL,
  `urutan` int DEFAULT "0",
  `parent_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT "0",
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_module`),
  KEY `module_FK` (`id_menu_grup`),
  CONSTRAINT `module_FK` FOREIGN KEY (`id_menu_grup`) REFERENCES `zeta_menu_grup` (`id_menu_grup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zeta_module`
--

LOCK TABLES `zeta_module` WRITE;
/*!40000 ALTER TABLE `zeta_module` DISABLE KEYS */;
INSERT INTO `zeta_module` VALUES ("0064bd20-0239-4380-a7c6-4c94e137b94d","Log","fa fa-history","log",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",99,"0","2021-09-11 18:22:29","2023-07-09 13:43:40","2023-07-09 13:43:40"),("03e9c167-7b6e-455f-9415-ff9ec4d7b017","02020","fa fa-users","qwdqwd",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",1,"0","2021-09-10 07:09:09","2021-09-10 07:31:12","2021-09-10 07:31:12"),("1563e47d-c506-4073-ae61-55922f7b3d78","Users","fa fa-angle-right","users",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",8,"0","2021-08-31 23:55:00","2021-09-10 07:38:48","2021-09-10 07:38:48"),("19680a96-e562-4980-9212-d3e9f9776618","dwd","dwdw","sds",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",2,"262d82ad-43c3-45cd-a027-7e4a7657b504","2023-07-09 13:45:25","2023-07-09 13:45:28","2023-07-09 13:45:28"),("20d07a91-8960-40cf-85bd-069e098cffa5","swd","fa fa-folder","2wd",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",2,"0","2023-07-09 13:45:00","2023-07-09 13:45:08","2023-07-09 13:45:08"),("262d82ad-43c3-45cd-a027-7e4a7657b504","Jenis Dokumen","icon-feather-folder","ref-jns-dokumen",1,"fe73d568-a426-4753-aa6a-fccec90a6ce0",1,"0","2021-09-20 01:24:16","2023-07-09 13:36:12",NULL),("2c0747d9-74f0-438e-82fe-6361cad5cc12","dwd","fa fa-folder","tes",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",2,"0","2023-07-23 08:51:14","2023-07-23 08:51:22","2023-07-23 08:51:22"),("3be55c7b-e194-4691-95e1-450964f05678","Menu","icon-feather-menu","module",1,"83258632-3d71-4478-aa64-b1bf62345770",2,"0","2021-08-31 23:55:00","2023-07-09 13:28:07",NULL),("3e469998-af47-4b9d-88f9-463cada7206a","Dashboard","icon-feather-home","dashboard",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",1,"0","2021-08-26 21:24:42","2023-07-09 13:37:27",NULL),("3e469998-af47-4b9d-88f9-463cada7406a","Book","fa fa-folder","book",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",1,"0","2021-08-26 21:24:42","2021-09-11 18:21:03","2021-09-11 18:21:03"),("3e469998-af47-4b9d-88f9-463cada7407a","Book","fa fa-folder","book",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",1,"3e469998-af47-4b9d-88f9-463cada7406a","2021-08-26 21:24:42","2021-08-26 21:24:42","2023-07-22 09:02:24"),("474d9d4c-e803-4725-a0d1-e26bf6bc05ec","Menu Grup","icon-feather-layers","menu-grup",1,"83258632-3d71-4478-aa64-b1bf62345770",1,"0","2021-08-31 23:55:00","2023-07-09 13:37:05",NULL),("4d55a183-fdd2-4df8-83aa-ea022c2771e0","wqdw","wqd","dwqd",1,"964988a5-a287-42f2-b61b-1caa3055657e",0,"0","2021-09-10 07:06:03","2021-09-10 07:06:51","2021-09-10 07:06:51"),("55e4e45a-1e63-429b-b2a1-f06ef4edbfac","User","fa fa-folder","users",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",1,"68a7a54b-5e3f-4569-aee4-89ab0d5c3ddb","2021-09-11 18:16:22","2021-09-11 18:16:22",NULL),("60f45852-0030-435e-957c-25aff1a20cac","wqewqe","wqew","wqeqw",1,"964988a5-a287-42f2-b61b-1caa3055657e",2,"03e9c167-7b6e-455f-9415-ff9ec4d7b017","2021-09-10 07:27:05","2021-09-10 07:31:03","2021-09-10 07:31:03"),("68a7a54b-5e3f-4569-aee4-89ab0d5c3ddb","Users","icon-feather-users","users",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",9,"0","2021-09-10 07:38:30","2023-07-09 13:40:59",NULL),("7346e8d7-d59c-4f32-925c-c55ed506f622","User","fa fa-folder","users",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",1,"68a7a54b-5e3f-4569-aee4-89ab0d5c3ddb","2021-09-11 11:14:43","2021-09-11 18:16:04","2021-09-11 18:16:04"),("85a2eb14-2aea-44cd-9c32-e9a454358dd2","Backup DB","fa fa-database","backup-db",1,"83258632-3d71-4478-aa64-b1bf62345770",4,"0","2021-09-23 00:45:10","2023-07-09 13:34:41","2023-07-09 13:34:41"),("9eb6f7cf-7914-40ab-8f7f-7c55945d858b","Role","fa fa-folder","role",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",2,"0","2021-09-10 07:41:21","2021-09-11 18:17:48","2021-09-11 18:17:48"),("a070d1b1-2f31-4de7-a2a2-b4f7d1a62b2c","Role Privilege","fa fa-folder","role-privilege",0,"0ee195cf-24b1-4a57-83fb-4addaebec367",3,"0","2021-09-10 08:03:28","2021-09-10 08:03:28",NULL),("a78e2b50-b302-41c5-975f-641f58ee757a","dwd","fa fa-folder","tes",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",2,"0","2023-07-23 08:44:57","2023-07-23 08:45:11","2023-07-23 08:45:11"),("d3d5f2d9-2e57-480f-85c2-287b4fc05c59","User","fa fa-folder","users",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",1,"68a7a54b-5e3f-4569-aee4-89ab0d5c3ddb","2021-09-11 11:11:33","2021-09-11 18:15:57","2021-09-11 18:15:57"),("d531c21b-70de-4ce5-99b9-eff0c80e2432","dwd abc","d","dwd",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",1,"0","2023-07-15 15:52:31","2023-07-15 15:52:42","2023-07-15 15:52:42"),("f4e73593-0eae-4d81-ab44-9d2e9ad5e96e","Role","fa fa-folder","role",1,"0ee195cf-24b1-4a57-83fb-4addaebec367",2,"68a7a54b-5e3f-4569-aee4-89ab0d5c3ddb","2021-09-11 18:17:13","2021-09-11 18:17:13",NULL),("f9c038ec-3792-427c-a3c7-65ea911c5af8","asasa","wqew","wqewq",0,"964988a5-a287-42f2-b61b-1caa3055657e",2,"03e9c167-7b6e-455f-9415-ff9ec4d7b017","2021-09-10 07:25:36","2021-09-10 07:30:58","2021-09-10 07:30:58");
/*!40000 ALTER TABLE `zeta_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zeta_password_resets`
--

DROP TABLE IF EXISTS `zeta_password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zeta_password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zeta_password_resets`
--

LOCK TABLES `zeta_password_resets` WRITE;
/*!40000 ALTER TABLE `zeta_password_resets` DISABLE KEYS */;
INSERT INTO `zeta_password_resets` VALUES ("admin@admin.com","Js9HQl5S6YD8iW8wVP9CLsNpOoH5P4QQjH1YJ1YUlyYX9QUnmdezzXTvDABNOf2t","2023-07-23 07:52:53");
/*!40000 ALTER TABLE `zeta_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zeta_ref_jns_dokumen`
--

DROP TABLE IF EXISTS `zeta_ref_jns_dokumen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zeta_ref_jns_dokumen` (
  `id_jns_dokumen` varchar(36) NOT NULL,
  `nm_jns_dokumen` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_jns_dokumen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zeta_ref_jns_dokumen`
--

LOCK TABLES `zeta_ref_jns_dokumen` WRITE;
/*!40000 ALTER TABLE `zeta_ref_jns_dokumen` DISABLE KEYS */;
INSERT INTO `zeta_ref_jns_dokumen` VALUES ("133174b3-eec3-42fd-b0b6-1286e086f079","Foto Profil","2021-09-20 01:29:57","2021-09-20 01:29:57",NULL),("2370795c-2b35-475f-a421-84dfef4830e1","fff","2021-09-23 01:44:37","2021-09-23 01:45:11","2021-09-23 01:45:11"),("271b0428-08f0-4788-8638-fdbb643b1512","wefwef","2021-09-23 01:52:20","2021-09-23 01:52:31","2021-09-23 01:52:31"),("35d31081-69dd-401c-ba84-42e12874f35d","tes ss","2023-07-23 08:11:28","2023-07-23 08:11:38","2023-07-23 08:11:38"),("746465da-42b9-4ddd-926f-ad6febeb5725","ddd","2021-09-22 01:23:45","2021-09-22 01:23:49","2021-09-22 01:23:49"),("8624bdcc-be01-4757-9900-e168dd84014e","dsfdsfe","2021-09-23 01:57:07","2021-09-23 01:57:10","2021-09-23 01:57:10"),("9dafa73d-d49b-415d-b489-425facb0f3f1","a","2023-07-09 14:51:53","2023-07-22 09:03:08","2023-07-22 09:03:08"),("a1259b45-40e9-4e03-8ad8-d026836fbc84","ddd d","2021-09-23 01:57:38","2021-09-23 01:57:46","2021-09-23 01:57:46"),("cc645996-d70a-4886-93ce-e5cb9d9a6865","aa","2023-07-09 14:49:40","2023-07-23 08:11:32","2023-07-23 08:11:32"),("d971568f-9014-452b-b8c3-83bd43d87136","wdw","2023-07-15 14:51:46","2023-07-15 14:57:20","2023-07-15 14:57:20");
/*!40000 ALTER TABLE `zeta_ref_jns_dokumen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zeta_role`
--

DROP TABLE IF EXISTS `zeta_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zeta_role` (
  `id_role` varchar(36) NOT NULL,
  `role_name` varchar(50) DEFAULT NULL,
  `role_slug` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zeta_role`
--

LOCK TABLES `zeta_role` WRITE;
/*!40000 ALTER TABLE `zeta_role` DISABLE KEYS */;
INSERT INTO `zeta_role` VALUES ("2ab93bfc-bbad-306b-b82c-ef4c397b6610","Admin","admin","2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("4dd35b56-cf8b-334f-a497-287ec47bdc04","Superadmin","superadmin","2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("55a6b7e6-8dd9-43af-ace2-55770ae2fc6d","dw","dwd","2023-07-23 09:05:56","2023-07-23 09:06:08","2023-07-23 09:06:08"),("bf727669-b882-4055-8a2b-baa2e98a29c8","dwdaa","dsdsd ddd","2023-07-15 15:56:42","2023-07-15 15:56:53","2023-07-15 15:56:53"),("e922113b-e013-4deb-a0e2-dd1462e5ddc7","dwd","dwd","2023-07-15 15:55:12","2023-07-15 15:57:00","2023-07-15 15:57:00");
/*!40000 ALTER TABLE `zeta_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zeta_role_privilege`
--

DROP TABLE IF EXISTS `zeta_role_privilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zeta_role_privilege` (
  `id_role_privilege` varchar(36) NOT NULL,
  `id_role` varchar(36) DEFAULT NULL,
  `id_module` varchar(36) DEFAULT NULL,
  `active` tinyint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_role_privilege`),
  KEY `role_privilege_FK` (`id_role`),
  KEY `role_privilege_FK_1` (`id_module`),
  CONSTRAINT `role_privilege_FK` FOREIGN KEY (`id_role`) REFERENCES `zeta_role` (`id_role`),
  CONSTRAINT `role_privilege_FK_1` FOREIGN KEY (`id_module`) REFERENCES `zeta_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zeta_role_privilege`
--

LOCK TABLES `zeta_role_privilege` WRITE;
/*!40000 ALTER TABLE `zeta_role_privilege` DISABLE KEYS */;
INSERT INTO `zeta_role_privilege` VALUES ("03355bcf-be0a-489e-915b-3d5136b80c61","4dd35b56-cf8b-334f-a497-287ec47bdc04","68a7a54b-5e3f-4569-aee4-89ab0d5c3ddb",1,"2021-09-11 08:40:18","2021-09-11 08:40:35",NULL),("41a2e8b0-fdae-4742-a609-2db19d96a03d","2ab93bfc-bbad-306b-b82c-ef4c397b6610","3e469998-af47-4b9d-88f9-463cada7407a",0,"2023-07-15 16:28:13","2023-07-22 16:01:10",NULL),("7615bd93-bb48-449e-9fa1-efcfe1250c0a","4dd35b56-cf8b-334f-a497-287ec47bdc04","f4e73593-0eae-4d81-ab44-9d2e9ad5e96e",1,"2021-09-11 18:17:23","2021-09-11 18:17:27",NULL),("8a40328c-4fc6-435c-9c96-b393bc369bd9","4dd35b56-cf8b-334f-a497-287ec47bdc04","0064bd20-0239-4380-a7c6-4c94e137b94d",1,"2021-09-11 18:22:38","2021-09-23 01:56:46",NULL),("a097e66e-5f4b-487a-beb4-15b94fa00581","4dd35b56-cf8b-334f-a497-287ec47bdc04","85a2eb14-2aea-44cd-9c32-e9a454358dd2",1,"2021-09-23 00:45:21","2021-09-23 00:45:23",NULL),("e24a61ea-cwec-3448-a7fd-0733badm384d","4dd35b56-cf8b-334f-a497-287ec47bdc04","9eb6f7cf-7914-40ab-8f7f-7c55945d858b",1,"2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("e24a61ea-cwec-3448-a7fd-0735badm384d","4dd35b56-cf8b-334f-a497-287ec47bdc04","a070d1b1-2f31-4de7-a2a2-b4f7d1a62b2c",1,"2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("e24a65ea-caec-3448-a7fd-0733bada384d","4dd35b56-cf8b-334f-a497-287ec47bdc04","3e469998-af47-4b9d-88f9-463cada7206a",1,"2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("e24a65ea-caec-3448-a7fd-0733badm384d","4dd35b56-cf8b-334f-a497-287ec47bdc04","474d9d4c-e803-4725-a0d1-e26bf6bc05ec",1,"2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("e24a65ea-caec-3448-a7fd-0733badt384d","4dd35b56-cf8b-334f-a497-287ec47bdc04","3e469998-af47-4b9d-88f9-463cada7407a",1,"2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("e24a65ea-caec-3448-a7fd-0733bafa382d","2ab93bfc-bbad-306b-b82c-ef4c397b6610","3e469998-af47-4b9d-88f9-463cada7406a",1,"2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("e24a65ea-caec-3448-a7fd-0733bafa384d","4dd35b56-cf8b-334f-a497-287ec47bdc04","3e469998-af47-4b9d-88f9-463cada7406a",1,"2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("e24a65ea-caec-3448-a7fd-0733cafa382d","2ab93bfc-bbad-306b-b82c-ef4c397b6610","3e469998-af47-4b9d-88f9-463cada7206a",1,"2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("e24a65ea-caec-3448-a7fs-0733cafa382d","2ab93bfc-bbad-306b-b82c-ef4c397b6610","3e469998-af47-4b9d-88f9-463cada7407a",1,"2021-08-26 21:36:09","2021-08-26 21:36:09","2021-08-26 21:36:09"),("e24a65ea-cwec-3448-a7fd-0733badm384d","4dd35b56-cf8b-334f-a497-287ec47bdc04","3be55c7b-e194-4691-95e1-450964f05678",1,"2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("e2e7013c-0e8b-4f04-a7d2-b4a5b022b4d3","2ab93bfc-bbad-306b-b82c-ef4c397b6610","3be55c7b-e194-4691-95e1-450964f05678",0,"2021-09-11 08:31:41","2021-09-11 08:40:08",NULL),("e4f51431-4108-4451-9b55-3cb6e4e908df","2ab93bfc-bbad-306b-b82c-ef4c397b6610","0064bd20-0239-4380-a7c6-4c94e137b94d",1,"2021-09-11 23:01:44","2021-09-11 23:01:46",NULL),("e9480740-255d-4cc2-8b86-27068ddad21b","4dd35b56-cf8b-334f-a497-287ec47bdc04","262d82ad-43c3-45cd-a027-7e4a7657b504",1,"2021-09-20 01:24:29","2021-09-20 01:24:33",NULL),("f33b7eeb-4ae3-4c55-840c-3cd7f22700b5","4dd35b56-cf8b-334f-a497-287ec47bdc04","55e4e45a-1e63-429b-b2a1-f06ef4edbfac",1,"2021-09-11 18:16:35","2021-09-11 18:16:38",NULL),("fdb0700c-29b8-41d3-acb2-da6eaaa3e309","2ab93bfc-bbad-306b-b82c-ef4c397b6610","262d82ad-43c3-45cd-a027-7e4a7657b504",0,"2023-07-22 16:06:51","2023-07-23 09:42:20",NULL);
/*!40000 ALTER TABLE `zeta_role_privilege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zeta_sessions`
--

DROP TABLE IF EXISTS `zeta_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zeta_sessions` (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_last_activity_index` (`last_activity`) USING BTREE,
  KEY `sessions_user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zeta_sessions`
--

LOCK TABLES `zeta_sessions` WRITE;
/*!40000 ALTER TABLE `zeta_sessions` DISABLE KEYS */;
INSERT INTO `zeta_sessions` VALUES ("LlZEbJILaXTiH1KiOSTLMw8T36BBEoBdZcUioqxF","3069bd28-7104-42e3-957f-65e8534e2fd7","127.0.0.1","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36","YTo5OntzOjY6Il90b2tlbiI7czo0MDoiU0dLQm1lWktTMFRQU2xGSEtZaHdta1BIRjNnOGVxOFBmNDczaWlrMiI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1MDoibG9naW5fd2ViXzU5YmEzNmFkZGMyYjJmOTQwMTU4MGYwMTRjN2Y1OGVhNGUzMDk4OWQiO3M6MzY6IjMwNjliZDI4LTcxMDQtNDJlMy05NTdmLTY1ZTg1MzRlMmZkNyI7czoxMzoicHJvZmlsZV9waG90byI7czo5NzoiaHR0cDovL2xvY2FsaG9zdDo4MDAwL3N0b3JhZ2UvdXBsb2Fkcy8yMDIzLzA3LzIzLzM3ODE4NDEyMTZfU2NyZWVuc2hvdF8yMDIzLTA3LTE5X2F0XzEyLjI3LjQ5LnBuZyI7czo1OiJyb2xlcyI7YToyOntpOjA7YTo0OntzOjc6ImlkX3JvbGUiO3M6MzY6IjJhYjkzYmZjLWJiYWQtMzA2Yi1iODJjLWVmNGMzOTdiNjYxMCI7czo5OiJyb2xlX25hbWUiO3M6NToiQWRtaW4iO3M6OToicm9sZV9zbHVnIjtzOjU6ImFkbWluIjtzOjU6InBpdm90IjthOjI6e3M6NzoiaWRfdXNlciI7czozNjoiMzA2OWJkMjgtNzEwNC00MmUzLTk1N2YtNjVlODUzNGUyZmQ3IjtzOjc6ImlkX3JvbGUiO3M6MzY6IjJhYjkzYmZjLWJiYWQtMzA2Yi1iODJjLWVmNGMzOTdiNjYxMCI7fX1pOjE7YTo0OntzOjc6ImlkX3JvbGUiO3M6MzY6IjRkZDM1YjU2LWNmOGItMzM0Zi1hNDk3LTI4N2VjNDdiZGMwNCI7czo5OiJyb2xlX25hbWUiO3M6MTA6IlN1cGVyYWRtaW4iO3M6OToicm9sZV9zbHVnIjtzOjEwOiJzdXBlcmFkbWluIjtzOjU6InBpdm90IjthOjI6e3M6NzoiaWRfdXNlciI7czozNjoiMzA2OWJkMjgtNzEwNC00MmUzLTk1N2YtNjVlODUzNGUyZmQ3IjtzOjc6ImlkX3JvbGUiO3M6MzY6IjRkZDM1YjU2LWNmOGItMzM0Zi1hNDk3LTI4N2VjNDdiZGMwNCI7fX19czoxMToiYWN0aXZlX3JvbGUiO2E6Nzp7czo3OiJpZF9yb2xlIjtzOjM2OiI0ZGQzNWI1Ni1jZjhiLTMzNGYtYTQ5Ny0yODdlYzQ3YmRjMDQiO3M6OToicm9sZV9uYW1lIjtzOjEwOiJTdXBlcmFkbWluIjtzOjk6InJvbGVfc2x1ZyI7czoxMDoic3VwZXJhZG1pbiI7czoxMDoiY3JlYXRlZF9hdCI7czoyNzoiMjAyMS0wOC0yNlQyMTozNjowOS4wMDAwMDBaIjtzOjEwOiJ1cGRhdGVkX2F0IjtzOjI3OiIyMDIxLTA4LTI2VDIxOjM2OjA5LjAwMDAwMFoiO3M6MTA6ImRlbGV0ZWRfYXQiO047czo1OiJwaXZvdCI7YToyOntzOjc6ImlkX3VzZXIiO3M6MzY6IjMwNjliZDI4LTcxMDQtNDJlMy05NTdmLTY1ZTg1MzRlMmZkNyI7czo3OiJpZF9yb2xlIjtzOjM2OiI0ZGQzNWI1Ni1jZjhiLTMzNGYtYTQ5Ny0yODdlYzQ3YmRjMDQiO319czo0OiJtZW51IjthOjE6e3M6OToiZ3J1cF9tZW51IjthOjM6e2k6MDthOjM6e3M6MjoiaWQiO3M6MzY6IjBlZTE5NWNmLTI0YjEtNGE1Ny04M2ZiLTRhZGRhZWJlYzM2NyI7czoyOiJubSI7czoxMDoiTWVudSBVdGFtYSI7czoxMDoibWVudV91dGFtYSI7YToyOntpOjA7YTo1OntzOjI6Im5tIjtzOjk6IkRhc2hib2FyZCI7czo0OiJpY29uIjtzOjE3OiJpY29uLWZlYXRoZXItaG9tZSI7czo0OiJzbHVnIjtzOjk6ImRhc2hib2FyZCI7czo3OiJzdWJtZW51IjtOO3M6MzoidXJsIjtzOjMxOiJodHRwOi8vbG9jYWxob3N0OjgwMDAvZGFzaGJvYXJkIjt9aToxO2E6NTp7czoyOiJubSI7czo1OiJVc2VycyI7czo0OiJpY29uIjtzOjE4OiJpY29uLWZlYXRoZXItdXNlcnMiO3M6NDoic2x1ZyI7czo1OiJ1c2VycyI7czo3OiJzdWJtZW51IjthOjI6e2k6MDthOjM6e3M6Mjoibm0iO3M6NDoiVXNlciI7czo0OiJpY29uIjtzOjEyOiJmYSBmYS1mb2xkZXIiO3M6MzoidXJsIjtzOjI3OiJodHRwOi8vbG9jYWxob3N0OjgwMDAvdXNlcnMiO31pOjE7YTozOntzOjI6Im5tIjtzOjQ6IlJvbGUiO3M6NDoiaWNvbiI7czoxMjoiZmEgZmEtZm9sZGVyIjtzOjM6InVybCI7czoyNjoiaHR0cDovL2xvY2FsaG9zdDo4MDAwL3JvbGUiO319czozOiJ1cmwiO3M6MToiIyI7fX19aToxO2E6Mzp7czoyOiJpZCI7czozNjoiODMyNTg2MzItM2Q3MS00NDc4LWFhNjQtYjFiZjYyMzQ1NzcwIjtzOjI6Im5tIjtzOjEwOiJQZW5nYXR1cmFuIjtzOjEwOiJtZW51X3V0YW1hIjthOjI6e2k6MDthOjU6e3M6Mjoibm0iO3M6OToiTWVudSBHcnVwIjtzOjQ6Imljb24iO3M6MTk6Imljb24tZmVhdGhlci1sYXllcnMiO3M6NDoic2x1ZyI7czo5OiJtZW51LWdydXAiO3M6Nzoic3VibWVudSI7TjtzOjM6InVybCI7czozMToiaHR0cDovL2xvY2FsaG9zdDo4MDAwL21lbnUtZ3J1cCI7fWk6MTthOjU6e3M6Mjoibm0iO3M6NDoiTWVudSI7czo0OiJpY29uIjtzOjE3OiJpY29uLWZlYXRoZXItbWVudSI7czo0OiJzbHVnIjtzOjY6Im1vZHVsZSI7czo3OiJzdWJtZW51IjtOO3M6MzoidXJsIjtzOjI4OiJodHRwOi8vbG9jYWxob3N0OjgwMDAvbW9kdWxlIjt9fX1pOjI7YTozOntzOjI6ImlkIjtzOjM2OiJmZTczZDU2OC1hNDI2LTQ3NTMtYWE2YS1mY2NlYzkwYTZjZTAiO3M6Mjoibm0iO3M6MTQ6IkRhdGEgUmVmZXJlbnNpIjtzOjEwOiJtZW51X3V0YW1hIjthOjE6e2k6MDthOjU6e3M6Mjoibm0iO3M6MTM6IkplbmlzIERva3VtZW4iO3M6NDoiaWNvbiI7czoxOToiaWNvbi1mZWF0aGVyLWZvbGRlciI7czo0OiJzbHVnIjtzOjE1OiJyZWYtam5zLWRva3VtZW4iO3M6Nzoic3VibWVudSI7TjtzOjM6InVybCI7czozNzoiaHR0cDovL2xvY2FsaG9zdDo4MDAwL3JlZi1qbnMtZG9rdW1lbiI7fX19fX1zOjE1OiJyb2xlX3ByaXZpbGVnZXMiO2E6Nzp7czoxNToicmVmLWpucy1kb2t1bWVuIjthOjE6e3M6NjoiYWN0aXZlIjtpOjE7fXM6NjoibW9kdWxlIjthOjE6e3M6NjoiYWN0aXZlIjtpOjE7fXM6OToiZGFzaGJvYXJkIjthOjE6e3M6NjoiYWN0aXZlIjtpOjE7fXM6OToibWVudS1ncnVwIjthOjE6e3M6NjoiYWN0aXZlIjtpOjE7fXM6NToidXNlcnMiO2E6MTp7czo2OiJhY3RpdmUiO2k6MTt9czoxNDoicm9sZS1wcml2aWxlZ2UiO2E6MTp7czo2OiJhY3RpdmUiO2k6MTt9czo0OiJyb2xlIjthOjE6e3M6NjoiYWN0aXZlIjtpOjE7fX1zOjQ6ImF1dGgiO2E6MTp7czoyMToicGFzc3dvcmRfY29uZmlybWVkX2F0IjtpOjE2OTAxMTMyMjc7fX0=",1690113230);
/*!40000 ALTER TABLE `zeta_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zeta_user_role`
--

DROP TABLE IF EXISTS `zeta_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zeta_user_role` (
  `id_user_role` varchar(36) NOT NULL,
  `id_role` varchar(36) DEFAULT NULL,
  `id_user` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_user_role`),
  KEY `user_role_FK` (`id_role`),
  KEY `user_role_FK_1` (`id_user`),
  CONSTRAINT `user_role_FK` FOREIGN KEY (`id_role`) REFERENCES `zeta_role` (`id_role`),
  CONSTRAINT `user_role_FK_1` FOREIGN KEY (`id_user`) REFERENCES `zeta_users` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zeta_user_role`
--

LOCK TABLES `zeta_user_role` WRITE;
/*!40000 ALTER TABLE `zeta_user_role` DISABLE KEYS */;
INSERT INTO `zeta_user_role` VALUES ("0f1355aa-def9-41ff-8ee9-81ce65a420c3","4dd35b56-cf8b-334f-a497-287ec47bdc04","3069bd28-7104-42e3-957f-65e8534e2fd7","2021-09-11 11:10:15","2023-07-23 07:40:34","2023-07-23 07:40:34"),("1d51002e-3c91-4f3c-ba10-f341bde6542d","2ab93bfc-bbad-306b-b82c-ef4c397b6610","c60e3150-e2e5-4467-b01d-877b950b5055","2023-07-15 15:54:35","2023-07-15 15:54:35",NULL),("1eebf215-7a44-44ac-9e60-c65214b80ef0","2ab93bfc-bbad-306b-b82c-ef4c397b6610","c60e3150-e2e5-4467-b01d-877b950b5055","2023-07-15 15:54:30","2023-07-15 15:54:35","2023-07-15 15:54:35"),("4dd35b56-cf8b-334f-a497-287ec47bdc24","4dd35b56-cf8b-334f-a497-287ec47bdc04","1","2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("4dd35b56-cf8b-334f-a497-287ec47wdc24","2ab93bfc-bbad-306b-b82c-ef4c397b6610","1","2021-08-26 21:36:09","2021-08-26 21:36:09",NULL),("4ffa5039-3296-4a92-b143-e8812a61e47b","2ab93bfc-bbad-306b-b82c-ef4c397b6610","3069bd28-7104-42e3-957f-65e8534e2fd7","2023-07-23 07:40:34","2023-07-23 07:40:34",NULL),("6001f85c-d546-4d13-8a2c-ffef906a61a4","2ab93bfc-bbad-306b-b82c-ef4c397b6610","19f43fe9-164e-401c-b800-3ae88bf44244","2023-07-23 07:40:18","2023-07-23 07:40:18",NULL),("61d20588-cd3a-4192-bd1a-b1b096895e1d","2ab93bfc-bbad-306b-b82c-ef4c397b6610","19f43fe9-164e-401c-b800-3ae88bf44244","2023-07-23 07:40:08","2023-07-23 07:40:18","2023-07-23 07:40:18"),("635c3a07-c5d0-4299-80f9-91c5836b945f","2ab93bfc-bbad-306b-b82c-ef4c397b6610","d762b9e0-891e-46a4-b86a-7d86550489f6","2021-09-11 11:08:01","2021-09-11 11:08:11","2021-09-11 11:08:11"),("699f5f12-a16c-4c46-87bc-67a46c7d3e4d","2ab93bfc-bbad-306b-b82c-ef4c397b6610","c8c549d2-dbd8-4225-a527-7e1e930c35f3","2021-09-11 10:26:09","2021-09-11 10:26:09",NULL),("6c560a0f-eb28-4cd5-9431-dd37cd9e37ce","4dd35b56-cf8b-334f-a497-287ec47bdc04","3069bd28-7104-42e3-957f-65e8534e2fd7","2023-07-23 07:40:34","2023-07-23 07:40:34",NULL),("75669b59-5bd2-4a28-b155-e5c2d3d51baf","4dd35b56-cf8b-334f-a497-287ec47bdc04","d762b9e0-891e-46a4-b86a-7d86550489f6","2021-09-11 11:09:39","2021-09-11 11:09:45","2021-09-11 11:09:45"),("7afce3ba-be29-470c-860b-e836d90a0ca0","2ab93bfc-bbad-306b-b82c-ef4c397b6610","23bb7f20-0ce8-4ec9-a336-d77a269994d5","2023-07-23 09:36:21","2023-07-23 09:36:21",NULL),("7b67a6ca-cc95-48bf-8659-61cdc59b873b","2ab93bfc-bbad-306b-b82c-ef4c397b6610","23bb7f20-0ce8-4ec9-a336-d77a269994d5","2023-07-23 09:36:12","2023-07-23 09:36:21","2023-07-23 09:36:21"),("8395b9ee-5114-4ab8-bb04-2990a1e0fb91","4dd35b56-cf8b-334f-a497-287ec47bdc04","d762b9e0-891e-46a4-b86a-7d86550489f6","2021-09-11 11:08:11","2021-09-11 11:09:39","2021-09-11 11:09:39"),("8adc1aab-7b5f-416c-b8cc-557ad1e3885b","4dd35b56-cf8b-334f-a497-287ec47bdc04","d762b9e0-891e-46a4-b86a-7d86550489f6","2021-09-11 11:09:45","2021-09-11 11:09:45",NULL),("cdf5dcb3-d881-4665-9534-c483cd36f3e9","2ab93bfc-bbad-306b-b82c-ef4c397b6610","d762b9e0-891e-46a4-b86a-7d86550489f6","2021-09-11 11:08:11","2021-09-11 11:09:39","2021-09-11 11:09:39"),("db963f24-c3a8-446c-ab19-24741246f1b0","2ab93bfc-bbad-306b-b82c-ef4c397b6610","b8904842-fcbf-46f5-999b-22b344cf3cff","2023-07-23 09:06:35","2023-07-23 09:07:40","2023-07-23 09:07:40"),("dc6935e2-490a-46a4-952b-06d79330bcec","2ab93bfc-bbad-306b-b82c-ef4c397b6610","b8904842-fcbf-46f5-999b-22b344cf3cff","2023-07-23 09:07:40","2023-07-23 09:07:40",NULL),("e236e38a-7cb3-458e-9fa5-e96641a9c36c","4dd35b56-cf8b-334f-a497-287ec47bdc04","d762b9e0-891e-46a4-b86a-7d86550489f6","2021-09-11 11:08:01","2021-09-11 11:08:11","2021-09-11 11:08:11"),("e542ff71-1324-475b-b786-fd6350c337f8","4dd35b56-cf8b-334f-a497-287ec47bdc04","c8c549d2-dbd8-4225-a527-7e1e930c35f3","2021-09-11 10:26:09","2021-09-11 10:26:09",NULL),("e63b297e-6312-4585-8ca2-8174af21e861","4dd35b56-cf8b-334f-a497-287ec47bdc04","b8904842-fcbf-46f5-999b-22b344cf3cff","2023-07-23 09:07:40","2023-07-23 09:07:40",NULL),("f01ff450-a86d-431b-bf25-3a1b2f8e67a3","2ab93bfc-bbad-306b-b82c-ef4c397b6610","d762b9e0-891e-46a4-b86a-7d86550489f6","2021-09-11 10:26:34","2021-09-11 11:07:35","2021-09-11 11:07:35"),("f5c6904b-81de-49ee-9a61-77dc1ebc6eb6","2ab93bfc-bbad-306b-b82c-ef4c397b6610","3069bd28-7104-42e3-957f-65e8534e2fd7","2021-09-11 11:10:15","2023-07-23 07:40:34","2023-07-23 07:40:34");
/*!40000 ALTER TABLE `zeta_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `zeta_users`
--

DROP TABLE IF EXISTS `zeta_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `zeta_users` (
  `id_user` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `zeta_users`
--

LOCK TABLES `zeta_users` WRITE;
/*!40000 ALTER TABLE `zeta_users` DISABLE KEYS */;
INSERT INTO `zeta_users` VALUES ("061aedf8-5737-4a1d-a6ab-31324f80fb94","wdw","dwdw@dw.dwdw","$2y$10$6f2HiTWMnm9gqdE29qQZAOiMoFjmREgt6X/D8VyeFmUPhLeso4K6G",NULL,NULL,NULL,"2021-09-11 10:25:38","2021-09-11 10:26:15","2021-09-11 10:26:15"),("1","John","johndoe@example.com","$2y$10$N8qx.tvDZqnRsEPplqHoOu36eXc2BCxGL5eBuxY98m4gksMk6a8Sq",NULL,"CfvSaRymljawnWflRi6TbXJYinBxq6MNCHvgrKN5Kp5IF9fEgszUAdbPqTZm",NULL,"2021-08-19 08:39:22","2023-07-23 09:46:19","2023-07-23 09:46:19"),("19f43fe9-164e-401c-b800-3ae88bf44244","dwd abc","dwd@dw.ddd","$2y$10$yh9teSuCpQr/PW1RpeHXmOoEZlMAlwopWjf./IHfxNvf5hCJgAtze",NULL,NULL,NULL,"2023-07-23 07:40:08","2023-07-23 07:40:22","2023-07-23 07:40:22"),("2","Carmelo","crona.elvis@example.net","$2y$10$HOG0w2T2oWV7ABvfhzw1weGneCtikdtTXMdF.Iqifr3UHmZClqdN.",NULL,"UBpslLROuV",NULL,"2021-08-19 08:39:23","2021-09-11 09:17:40","2021-09-11 09:17:40"),("23bb7f20-0ce8-4ec9-a336-d77a269994d5","dwd","dwd@dw.dwd","$2y$10$v4PK6uXzFHrbmujMrxN2yeGNcbLk3F6vBTeE5wVM8BScMb5Onkz7q",NULL,NULL,NULL,"2023-07-23 09:36:12","2023-07-23 09:36:24","2023-07-23 09:36:24"),("3","Sheila","roosevelt22@example.com","$2y$10$MDW3WB78If.Ubs/Y4LcW7.1paTAwQ7qjmUnHPZ9LIt5P6GXUjKIkm",NULL,"nDKGXDdX5N",NULL,"2021-08-19 08:39:23","2021-09-11 09:17:44","2021-09-11 09:17:44"),("3069bd28-7104-42e3-957f-65e8534e2fd7","Hajul","admin@admin.com","$2y$10$ZhQK5lmsPFreyaRhQCJzieL8REbLDwpQW.yHHo55BgmefZiaJZtFS",NULL,"3ePnvP6MXBZHzrO6odsZAOtMoWzxHsz89XueerXIqkIYqWrun3aHwEwBO0Sw","2023-07-09 01:19:15","2021-09-11 11:10:15","2023-07-23 11:53:25",NULL),("4","Karine","everardo.miller@example.net","$2y$10$0zcYmFsuS0xPZvpaGDVGTOqX8ZGJRghg0PXOlKD6LXAWXvGkuy5py",NULL,"HVrlZ1KUI8",NULL,"2021-08-19 08:39:23","2021-09-11 09:17:47","2021-09-11 09:17:47"),("5","Alejandra","fpouros@example.org","$2y$10$HAhu0yRyYsheq5KT3GTs2.hWK4Y/jxjYFD7DHo/74hQ.4gpehowNu",NULL,"h8CNjHYBKZ",NULL,"2021-08-19 08:39:23","2021-09-11 09:17:49","2021-09-11 09:17:49"),("6","Elmo","rutherford.wellington@example.org","$2y$10$KUheq3j5k80uQedGNRH.eO2FHN5QwN5wOYB1vHfWKoEHaNfQqMapS",NULL,"8ZnY4uJMJbR5faGmtMsJ9APCPhTWDPaBlMQFIaVxzIL7UtHGd6SYAPFMp1Kj",NULL,"2021-08-19 08:39:23","2021-09-11 09:17:52","2021-09-11 09:17:52"),("6abb8a59-6f44-4773-b50a-fe7e86ab3ac7","dwdw","wdwd@dw.wdw","$2y$10$HWULCYTBcvAuvMWDWMbbqu102R2PErMSbILKmCWpl4mTel03IOXtG",NULL,NULL,NULL,"2021-09-11 10:19:33","2021-09-11 10:20:17","2021-09-11 10:20:17"),("6b5be2f0-00f1-4b5f-b3ed-024065722034","dwdw","dw@dwdw.dwdw","$2y$10$SLrfeYDcL0SWZFBbXIQ2a.yz.rjztz//i4iQQwcTcCgnEgTB3QfBq",NULL,NULL,NULL,"2021-09-11 10:13:42","2021-09-11 10:13:47","2021-09-11 10:13:47"),("7b6db6ec-a742-423b-b138-8d6c1b2cfe58","wdwd","wdw@wd.wdw","$2y$10$CTxy4.fBd8OzkhNWwxGdq.C/.IfNEIXB7kLuC.AbT73reQlg0IPvS",NULL,NULL,NULL,"2021-09-11 10:20:28","2021-09-11 10:26:18","2021-09-11 10:26:18"),("b8904842-fcbf-46f5-999b-22b344cf3cff","sss","ss@dw.dwd","$2y$10$Lg2xmGXN7xnJhT0rZV0TR.MMmAvI9nFcOa./T3v1sh6h3M.PD3zre",NULL,NULL,NULL,"2023-07-23 09:06:35","2023-07-23 09:07:44","2023-07-23 09:07:44"),("c60e3150-e2e5-4467-b01d-877b950b5055","wd dddd","dwd@wd.wd","$2y$10$eyjDovMUO3SZwkB/RGTmSO6AlFpwLTycvl7i47TehvdOUJ/5C43Ei",NULL,NULL,NULL,"2023-07-15 15:54:30","2023-07-15 15:54:40","2023-07-15 15:54:40"),("c8c549d2-dbd8-4225-a527-7e1e930c35f3","wdwd","wdw@w.wdwd","$2y$10$XjuowX00UO5rN7eeCYuOZ.qnoPf14xmNop6Yw5ZgcTMCkfA/mHzZS",NULL,NULL,NULL,"2021-09-11 10:26:09","2021-09-11 10:26:23","2021-09-11 10:26:23"),("d762b9e0-891e-46a4-b86a-7d86550489f6","dwdw wdwd","sqwsw@wd.wdw","$2y$10$bW8LlLuQmEhZe/qy1Tv0h.o83drNqcr0Q/ZVIRff.gljbqJzkXLoG",NULL,NULL,NULL,"2021-09-11 10:26:34","2021-09-11 11:09:55","2021-09-11 11:09:55");
/*!40000 ALTER TABLE `zeta_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database "zmanager"
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-23 18:55:34
