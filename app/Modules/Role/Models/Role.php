<?php

namespace App\Modules\Role\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \App\Bardiz12\Eloquent\UseUUID;
use App\Bardiz12\BaseModel;

class Role extends BaseModel{
    use UseUUID;
    protected $table = 'zeta_role';
    protected $primaryKey = "id_role";
    protected $fillable = [
        "role_name",
        "role_slug"
    ];

    protected $relationField = [

    ];
    
    protected $relationFieldInject = [

    ];

    public function modules() {
        return $this
        ->belongsToMany( 'App\Modules\Module\Models\Module', 'zeta_role_privilege', 'id_role', 'id_module' )
        ->withPivot( [
            'active',
        ] );
    }

    public function rolePrivileges() {
        return $this->hasMany( 'App\Modules\Module\Models\RolePrivilege', 'id_role', 'id_role' );
    }
}