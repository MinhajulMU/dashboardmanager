import React, { useState } from "react";
import Layout from "@/Pages/Layout";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
import FlashMessages from "@/Shared/components/FlashMessages";
import DatatableHeader from "@/Shared/components/DatatableHeader";
import Pagination from "@/Shared/components/Pagination";
import FilterDatatable from "@/Shared/components/FilterDatatable";
import DeleteModal from "@/Shared/components/DeleteModal";
import { Inertia } from "@inertiajs/inertia";
import Sadrow from "@/Shared/components/Sadrow";
import Action from "@/Shared/components/Action/Action";
const Index = () => {
    const props = usePage().props;
    const { headerField, data, order_field, order_mode } = usePage().props;
    const [deleteModal, setDeleteModal] = useState({
        show: false,
        url: "",
    });

    const handleCloseDeleteModal = () =>
        setDeleteModal({ ...deleteModal, show: false });
    const handleShowDeleteModal = (deleteUrl) => {
        setDeleteModal({
            url: deleteUrl,
            show: true,
        });
    };
    const handleDeleteAction = () => {
        Inertia.delete(deleteModal.url);
        setDeleteModal({
            show: false,
        });
    };

    return (
        <React.Fragment>
            <div className="container mx-auto min-h-screen">
                <div className="md:-mb-3 space-y-2 mt-16">
                    <div className="grid grid-cols-2">
                        <div>
                            <h1 className="font-semibold md:text-2xl text-lg">
                                Jenis Dokumen
                            </h1>
                        </div>
                        <div className="text-right"></div>
                    </div>
                </div>
                <div className="card mt-5">
                    <div className="grid grid-cols-2 p-5 pt-8">
                        <div>
                            <h3 className="font-semibold text-lg capitalize">
                                Manajemen Data Jenis Dokumen
                            </h3>
                            <h4 className="font-medium  text-gray-600">
                                Tabel Jenis Dokumen
                            </h4>
                        </div>
                        <div className="text-right">
                            <InertiaLink href={route("ref-jns-dokumen.create")}>
                                <button className="button ml-auto">
                                    <i className="fa fa-plus mr-1"></i> Tambah
                                    Jenis Dokumen
                                </button>
                            </InertiaLink>
                        </div>
                    </div>
                    <div className="table-responsive mt-5 pt-2">
                        <div className="px-5">
                            <FlashMessages />
                            <FilterDatatable></FilterDatatable>
                        </div>
                        <table id="add-row" className="w-full border-b">
                            <thead>
                                <DatatableHeader
                                    headerField={headerField}
                                    order_field={order_field}
                                    order_mode={order_mode}
                                ></DatatableHeader>
                            </thead>
                            <tbody>
                                {data.data.length > 0 ? (
                                    data.data.map((items, index) => (
                                        <tr key={index}>
                                            <td className="p-3">
                                                {(data.current_page - 1) *
                                                    data.per_page +
                                                    index +
                                                    1}
                                            </td>
                                            <td className="p-3">
                                                {items.nm_jns_dokumen}
                                            </td>
                                            <td className="p-3">
                                                <Action
                                                    module="ref-jns-dokumen"
                                                    detail_url={route(
                                                        "ref-jns-dokumen.show",
                                                        items.id_jns_dokumen
                                                    )}
                                                    edit_url={route(
                                                        "ref-jns-dokumen.edit",
                                                        items.id_jns_dokumen
                                                    )}
                                                    delete_url={route(
                                                        "ref-jns-dokumen.destroy",
                                                        items.id_jns_dokumen
                                                    ).toString()}
                                                    handleShowDeleteModal={
                                                        handleShowDeleteModal
                                                    }
                                                ></Action>
                                            </td>
                                        </tr>
                                    ))
                                ) : (
                                    <Sadrow></Sadrow>
                                )}
                            </tbody>
                        </table>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <Pagination
                                            data={data.links}
                                            from={data.from}
                                            to={data.to}
                                            total={data.total}
                                        ></Pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <DeleteModal
                show={deleteModal.show}
                handleCloseDeleteModal={handleCloseDeleteModal}
                handleDeleteAction={handleDeleteAction}
            ></DeleteModal>
        </React.Fragment>
    );
};

Index.layout = (page) => <Layout children={page} title="Jenis Dokumen" />;
export default Index;
