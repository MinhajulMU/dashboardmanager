import React, { useState, useEffect, Fragment } from "react";
import Layout from "@/Pages/Layout";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
import FlashMessages from "@/Shared/components/FlashMessages";
import DatatableHeader from "@/Shared/components/DatatableHeader";
import Pagination from "@/Shared/components/Pagination";
import FilterDatatable from "@/Shared/components/FilterDatatable";
import Action from "@/Shared/components/Action/Action";
import Sadrow from "@/Shared/components/Sadrow";
import DeleteModal from "@/Shared/components/DeleteModal";
import { Inertia } from "@inertiajs/inertia";
import BackButton from "@/Shared/components/BackButton";
import { Button, Modal } from "react-bootstrap";

const Index = () => {
    const props = usePage().props;
    const { headerField, data, order_field, order_mode } = usePage().props;
    const [deleteModal, setDeleteModal] = useState({
        show: false,
        url: "",
    });
    const handleCloseDeleteModal = () =>
        setDeleteModal({ ...deleteModal, show: false });
    const handleShowDeleteModal = (deleteUrl) => {
        setDeleteModal({
            url: deleteUrl,
            show: true,
        });
    };
    const handleDeleteAction = () => {
        Inertia.delete(deleteModal.url);
        setDeleteModal({
            show: false,
        });
    };

    return (
        <React.Fragment>
            <div className="container mx-auto min-h-screen">
                {/*  breadcrumb */}
                <div className="md:-mb-3 space-y-2 mt-16">
                    <div className="grid grid-cols-2">
                        <div>
                            <h1 className="font-semibold md:text-2xl text-lg">
                                {" "}
                                Module
                            </h1>
                        </div>
                        <div className="text-right">
                            {props.parent_id != 0 && (
                                <BackButton>{route("module.index")}</BackButton>
                            )}
                        </div>
                    </div>
                </div>
                <div className="card mt-5">
                    <div className="grid grid-cols-2 p-5 pt-8">
                        <div>
                            <h3 className="font-semibold text-lg capitalize">
                                Manajemen Data Module
                            </h3>
                            <h4 className="font-medium  text-gray-600">
                                Tabel Module
                            </h4>
                        </div>
                        <div className="text-right">
                            <InertiaLink
                                href={
                                    route("module.create") +
                                    "?parent_id=" +
                                    props.parent_id
                                }
                            >
                                <button className="button btn-round ml-auto">
                                    <i className="icon-feather-plus"></i> Tambah
                                    Module
                                </button>
                            </InertiaLink>
                        </div>
                    </div>

                    <div className="table-responsive mt-5 pt-2">
                        <div className="px-5">
                            <FlashMessages />
                            <FilterDatatable></FilterDatatable>
                        </div>
                        <table id="add-row" className="w-full border-b">
                            <thead>
                                <DatatableHeader
                                    headerField={headerField}
                                    order_field={order_field}
                                    order_mode={order_mode}
                                ></DatatableHeader>
                            </thead>
                            <tbody>
                                {data.data.length > 0 ? (
                                    data.data.map((items, index) => (
                                        <tr key={index}>
                                            <td className="p-3">
                                                {(data.current_page - 1) *
                                                    data.per_page +
                                                    index +
                                                    1}
                                            </td>
                                            <td className="p-3">
                                                {items.name}
                                            </td>
                                            <td className="p-3">
                                                {items.id_menu_grup}
                                            </td>
                                            <td className="p-3">
                                                {items.urutan}
                                            </td>
                                            <td className="p-3">
                                                <i className={items.icon}></i>{" "}
                                            </td>
                                            <td className="p-3">
                                                {items.is_show == 1
                                                    ? "Ya"
                                                    : "Tidak"}
                                            </td>
                                            <td className="p-3">
                                                {props.parent_id == 0 ? (
                                                    <InertiaLink
                                                        href={
                                                            route(
                                                                "module.index"
                                                            ) +
                                                            "?parent_id=" +
                                                            items.id_module
                                                        }
                                                    >
                                                        <button className="button small">
                                                            <span className="icon-feather-menu mr-1"></span>{" "}
                                                            Submenu
                                                        </button>
                                                    </InertiaLink>
                                                ) : (
                                                    <button className="button dark small">
                                                        <span className="icon-feather-menu mr-1"></span>{" "}
                                                        Submenu
                                                    </button>
                                                )}
                                            </td>
                                            <td className="p-3">
                                                <Action
                                                    module="module"
                                                    detail_url={route(
                                                        "module.show",
                                                        {
                                                            module: items.id_module,
                                                        }
                                                    )}
                                                    delete_url={route(
                                                        "module.destroy",
                                                        items.id_module
                                                    ).toString()}
                                                    edit_url={route(
                                                        "module.edit",
                                                        items.id_module
                                                    )}
                                                    handleShowDeleteModal={
                                                        handleShowDeleteModal
                                                    }
                                                ></Action>
                                            </td>
                                        </tr>
                                    ))
                                ) : (
                                    <Sadrow></Sadrow>
                                )}
                            </tbody>
                        </table>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <Pagination
                                            data={data.links}
                                            from={data.from}
                                            to={data.to}
                                            total={data.total}
                                        ></Pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <DeleteModal
                show={deleteModal.show}
                handleCloseDeleteModal={handleCloseDeleteModal}
                handleDeleteAction={handleDeleteAction}
            ></DeleteModal>
        </React.Fragment>
    );
};

Index.layout = (page) => <Layout children={page} title="Module" />;
export default Index;
