import React, { useState } from "react";
import Layout from "@/Pages/Layout";
import { InertiaLink, usePage, useForm } from "@inertiajs/inertia-react";
import TextInput from "@/Shared/Form/TextInput";
import TextareaInput from "@/Shared/Form/TextareaInput";
import Select2 from "@/Shared/Form/Select2";
import FlashMessages from "@/Shared/components/FlashMessages";
import BackButton from "@/Shared/components/BackButton";
import Radio from "@/Shared/Form/Radio";
import SubmitButton from "@/Shared/Form/SubmitButton";
import Form from "../../../Modules/Module/Inertia/Form";

const Index = () => {
  const props = usePage().props;
  const { data, setData, errors, post, processing } = useForm({
    name: "",
    icon: "",
    slug: "",
    is_show: "",
    id_menu_grup: "",
    urutan: "",
    parent_id: props.parent_id,
  });
  function handleSubmit(e) {
    e.preventDefault();
    post(route("module.store"));
  }

  return (
    <React.Fragment>
      <div className="container mx-auto min-h-screen">
        <div className="md:-mb-3 space-y-2 mt-16">
          <div className="grid grid-cols-2">
            <div>
              <h1 className="font-semibold md:text-2xl text-lg">
                Tambah Module
              </h1>
            </div>
            <div className="text-right">
              <BackButton>{props.routes.backUrl}</BackButton>
            </div>
          </div>
          <FlashMessages />
        </div>
        <div className="card mt-5">
          <div className="grid grid-cols-2 p-5 pt-8">
            <div>
              <h3 className="font-semibold text-lg capitalize">Form Module</h3>
              <p>Isi semua form dibawah dengan benar</p>
            </div>
            <div className="text-right"></div>
          </div>
          <form onSubmit={handleSubmit}>
              <Form
                data={data}
                setData={setData}
                processing={processing}
              ></Form>
            </form>
        </div>
      </div>
    </React.Fragment>
  );
};

Index.layout = (page) => <Layout children={page} title="Tambah Module" />;
export default Index;
