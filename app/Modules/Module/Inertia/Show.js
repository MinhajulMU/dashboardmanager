import React, { useState } from "react";
import Layout from "@/Pages/Layout";
import { InertiaLink, usePage, useForm } from "@inertiajs/inertia-react";
import BackButton from "@/Shared/components/BackButton";

const Show = ({ data, routes }) => {
  return (
    <React.Fragment>
      <div className="container mx-auto min-h-screen">
        <div className="md:-mb-3 space-y-2 mt-16">
          <div className="grid grid-cols-2">
            <div>
              <h1 className="font-semibold md:text-2xl text-lg">
                {" "}
                Detail Module
              </h1>
            </div>
            <div className="text-right">
              <BackButton>{routes.backUrl}</BackButton>
            </div>
          </div>
        </div>
        <div className="card mt-5">
          <div className="grid grid-cols-2 p-5 pt-8">
            <div>
              <h3 className="font-semibold text-lg capitalize">
                Detail Rincian Data
              </h3>
            </div>
            <div className="text-right"></div>
          </div>
          <div className="container">
            <table>
              <tbody>
                <tr>
                  <td className="px-4 py-2 align-top">Name</td>
                  <td className="px-4 py-2 align-top" width="10px">
                    :
                  </td>
                  <td className="px-4 py-2">{data.name}</td>
                </tr>

                <tr>
                  <td className="px-4 py-2 align-top">Icon</td>
                  <td className="px-4 py-2 align-top" width="10px">
                    :
                  </td>
                  <td className="px-4 py-2"><span className={data.icon}></span> </td>
                </tr>

                <tr>
                  <td className="px-4 py-2 align-top">Slug</td>
                  <td className="px-4 py-2 align-top" width="10px">
                    :
                  </td>
                  <td className="px-4 py-2">{data.slug}</td>
                </tr>

                <tr>
                  <td className="px-4 py-2 align-top">Is Show</td>
                  <td className="px-4 py-2 align-top" width="10px">
                    :
                  </td>
                  <td className="px-4 py-2">{data.is_show == 1 ? "Ya" : "Tidak"}</td>
                </tr>

                <tr>
                  <td className="px-4 py-2 align-top">Menu Grup</td>
                  <td className="px-4 py-2 align-top" width="10px">
                    :
                  </td>
                  <td className="px-4 py-2">{data.id_menu_grup}</td>
                </tr>

                <tr>
                  <td className="px-4 py-2 align-top">Urutan</td>
                  <td className="px-4 py-2 align-top" width="10px">
                    :
                  </td>
                  <td className="px-4 py-2">{data.urutan}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

Show.layout = (page) => <Layout children={page} title="Detail Module" />;
export default Show;
