<?php

namespace App\Modules\RolePrivilege\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Log;
use App\Modules\RolePrivilege\Models\RolePrivilege;
use App\Modules\Module\Models\Module;
use App\Modules\Role\Models\Role;

class RolePrivilegeController extends Controller
{
    protected $title = "Role Privilege";
    protected $model;
    public function __construct()
    {
        $this->model = new RolePrivilege();
    }

    public function index(Request $request)
    {
        $id_role = $request->input('id_role');
        $data['role'] = Role::find($id_role);
        $search = $request->input('search');
        $data['search'] = $search;
        $keywords = [];
        if ($search != null) {
            $keywords['name'] = $search;
        }
        $module = Module::where(function ($w) use ($keywords) {
            foreach ($keywords as $k => $v) {
                $w->where($k, 'like', '%' . $v . '%');
            }
        })->orderBy('name', 'asc')->get();
        $modules = [];
        foreach ($module as $item) {
            $slug = $item->id_module;
            $privileges = $item->rolePrivileges($id_role);
            $modules[$slug] = $item->toArray();
            $modules[$slug]['privileges'] = [
                'active' => ($privileges->active ?? 0) == 1
            ];
        }
        $data['data'] = $modules;
        return Inertia::render('RolePrivilege::Index', $data);
    }
    public function create()
    {
        $data['routes']['backUrl'] =  URL::route('role-privilege.index');
        $data["ref_module"] = Module::get(["id_module as value", "icon as label"]);
        $data["ref_role"] = Role::get(["id_role as value", "role_name as label"]);
        return Inertia::render('RolePrivilege::Create', $data);
    }
    public function store(Request $request)
    {
        $rolePrivilege = RolePrivilege::where('id_module', $request->input('id_module'))->where('id_role', $request->input('id_role'))->first();
        if ($rolePrivilege === null) {
            $rolePrivilege = RolePrivilege::create([
                'id_role' => $request->input('id_role'),
                'id_module' => $request->input('id_module'),
                'active'  => 1
            ]);
        } else {
            $rolePrivilege->update([
                "active" => !$rolePrivilege->{$request->input('action')}
            ]);
        }
        return response()->json([
            'success' => true
        ]);
    }

}
