<?php

namespace App\Modules\RolePrivilege\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \App\Bardiz12\Eloquent\UseUUID;
use App\Bardiz12\BaseModel;

class RolePrivilege extends BaseModel{
    use UseUUID;
    protected $table = 'zeta_role_privilege';
    protected $primaryKey = "id_role_privilege";
    protected $fillable = [
        "active",
        "id_module",
        "id_role"
    ];

    protected $relationField = [
        "id_role" => "zeta_role.id_role",
        "id_module" => "module.id_module"
    ];
    
    protected $relationFieldInject = [
        "id_role" => "id_role",
    ];
    
    public function module()
    {
        return $this->hasOne('App\Modules\Module\Models\Module', 'id_module', 'id_module');
    }
    
}