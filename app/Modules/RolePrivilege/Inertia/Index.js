import React, { useState, useEffect } from "react";
import Layout from "@/Pages/Layout";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
import FlashMessages from "@/Shared/components/FlashMessages";
import DatatableHeader from "@/Shared/components/DatatableHeader";
import Pagination from "@/Shared/components/Pagination";
import FilterDatatable from "@/Shared/components/FilterDatatable";
import DeleteModal from "@/Shared/components/DeleteModal";
import BackButton from "@/Shared/components/BackButton";
import { Inertia } from "@inertiajs/inertia";
import { usePrevious } from "react-use";
import axios from "axios";
import Sadrow from "@/Shared/components/Sadrow";
import Swal from "sweetalert2";

const Index = () => {
  const props = usePage().props;
  const Toast = Swal.mixin({
    toast: true,
    position: "top",
    showConfirmButton: false,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener("mouseenter", Swal.stopTimer);
      toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
  });
  const urlSearchParams = new URLSearchParams(window.location.search);
  const params = Object.fromEntries(urlSearchParams.entries());
  delete params["search"];
  const [deleteModal, setDeleteModal] = useState({
    show: false,
    url: "",
  });
  const [values, setValues] = useState({
    search: props.search || "",
  });
  const prevValues = usePrevious(values);
  useEffect(() => {
    const query = values;
    if (prevValues) {
      Inertia.get(
        route(route().current()),
        { ...values, ...params },
        {
          replace: true,
          preserveState: true,
        }
      );
    }
  }, [values]);
  const handleChange = (e) => {
    const key = e.target.name;
    const value = e.target.value;
    setValues((values) => ({
      ...values,
      [key]: value,
    }));
  };
  const handleCloseDeleteModal = () =>
    setDeleteModal({ ...deleteModal, show: false });
  const handleShowDeleteModal = (deleteUrl) => {
    setDeleteModal({
      url: deleteUrl,
      show: true,
    });
  };
  const handleDeleteAction = () => {
    Inertia.delete(deleteModal.url);
    setDeleteModal({
      show: false,
    });
  };

  const showNotification = (icon = null, title = null, timer = null) => {
    Toast.fire({
      icon: icon || "success",
      title: title || "Berhasil",
      timer: timer === null ? 3000 : timer * 1000,
    });
  };
  const togglePermission = (id_module, action) => {
    axios
      .post(route("role-privilege.store"), {
        _token: props.csrf,
        id_module: id_module,
        action: action,
        id_role: props.role.id_role,
      })
      .then((res) => {
        if (res.data.success == true) {
          showNotification(null, "Berhasil merubah data!", null);
        } else {
          showNotification("error", "Gagal merubah data!", null);
        }
        Inertia.get(
          route(route().current()),
          { ...values, ...params },
          {
            replace: true,
            preserveState: true,
          }
        );
      });
  };

  return (
    <React.Fragment>
      <div className="container mx-auto min-h-screen">
        <div className="md:-mb-3 space-y-2 mt-16">
          <div className="grid grid-cols-2">
            <div>
              <h1 className="font-semibold md:text-2xl text-lg">
                Role Privilege
              </h1>
            </div>
            <div className="text-right"></div>
          </div>
        </div>
        <div className="card mt-5 pb-5">
          <div className="grid grid-cols-2 p-5 pt-8">
            <div>
              <h3 className="font-semibold text-lg capitalize">
                Daftar privileges untuk Role : <b>{props.role.role_name}</b>
              </h3>
            </div>
            <div className="text-right">
              <BackButton>{route("role.index")}</BackButton>
            </div>
          </div>
          <div className="px-5">
            <input
              type="text"
              name="search"
              className="shadow-none with-border w-full"
              placeholder="Search Module ..."
              value={values.search}
              onChange={handleChange}
            ></input>
            <table id="add-row" className="w-full mt-4">
              <thead>
                <tr>
                  <th className="p-3">Module</th>
                  <th className="text-right p-3">Aktif ?</th>
                </tr>
              </thead>
              <tbody>
                {props.data != 0 ? (
                  Object.keys(props.data).map(function (index) {
                    return (
                      <tr key={index}>
                        <td className="p-3">{props.data[index].name} </td>
                        {Object.keys(props.data[index].privileges).map(
                          function (key) {
                            return (
                              <React.Fragment key={key}>
                                <td className="p-3 text-right">
                                  {props.data[index].privileges[key] != true ? (
                                    <button
                                      className="button small danger"
                                      onClick={() =>
                                        togglePermission(
                                          props.data[index].id_module,
                                          key
                                        )
                                      }
                                    >
                                      <i className="fa fa-times mr-1"></i>{" "}
                                      Disabled
                                    </button>
                                  ) : (
                                    <button
                                      className="button small success"
                                      onClick={() =>
                                        togglePermission(
                                          props.data[index].id_module,
                                          key
                                        )
                                      }
                                    >
                                      <i className="fa fa-check mr-1"></i>{" "}
                                      Enabled
                                    </button>
                                  )}
                                </td>
                              </React.Fragment>
                            );
                          }
                        )}
                      </tr>
                    );
                  })
                ) : (
                  <Sadrow></Sadrow>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <DeleteModal
        show={deleteModal.show}
        handleCloseDeleteModal={handleCloseDeleteModal}
        handleDeleteAction={handleDeleteAction}
      ></DeleteModal>
    </React.Fragment>
  );
};

Index.layout = (page) => <Layout children={page} title="Role Privilege" />;
export default Index;
