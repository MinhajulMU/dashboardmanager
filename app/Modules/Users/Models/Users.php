<?php

namespace App\Modules\Users\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use \App\Bardiz12\Eloquent\UseUUID;
use App\Bardiz12\BaseModel;

class Users extends BaseModel{
    use UseUUID;
    protected $table = 'zeta_users';
    protected $primaryKey = "id_user";
    protected $fillable = [
        "name",
        "email",
        "password",
        "photo_path",
        "remember_token",
        'email_verified_at'
    ];

    protected $relationField = [

    ];
    
    protected $relationFieldInject = [

    ];
    
    public function roles()
    {
        return $this->belongsToMany('App\Modules\Role\Models\Role', 'zeta_user_role', 'id_user', 'id_role')->wherePivot('deleted_at',null);
    }

    public function foto()
    {
        return $this->hasMany('App\Modules\Dokumen\Models\Dokumen','id_model','id_user')->where('zeta_dokumen.id_jns_dokumen','133174b3-eec3-42fd-b0b6-1286e086f079')->orderBy('created_at','desc');
    }
    
}