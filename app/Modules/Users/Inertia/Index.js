import React, { useState } from "react";
import Layout from "@/Pages/Layout";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
import FlashMessages from "@/Shared/components/FlashMessages";
import DatatableHeader from "@/Shared/components/DatatableHeader";
import Pagination from "@/Shared/components/Pagination";
import FilterDatatable from "@/Shared/components/FilterDatatable";
import Action from "@/Shared/components/Action/Action";
import DeleteModal from "@/Shared/components/DeleteModal";
import { Inertia } from "@inertiajs/inertia";

const Index = () => {
    const props = usePage().props;
    const { headerField, data, order_field, order_mode } = usePage().props;
    const [deleteModal, setDeleteModal] = useState({
        show: false,
        url: "",
    });

    const handleCloseDeleteModal = () =>
        setDeleteModal({ ...deleteModal, show: false });
    const handleShowDeleteModal = (deleteUrl) => {
        setDeleteModal({
            url: deleteUrl,
            show: true,
        });
    };
    const handleDeleteAction = () => {
        Inertia.delete(deleteModal.url);
        setDeleteModal({
            show: false,
        });
    };

    return (
        <React.Fragment>
            <div className="container mx-auto min-h-screen">
                <div className="md:-mb-3 space-y-2 mt-16">
                    <div className="grid grid-cols-2">
                        <div>
                            <h1 className="font-semibold md:text-2xl text-lg">
                                Users
                            </h1>
                        </div>
                        <div className="text-right"></div>
                    </div>
                </div>
                <div className="card mt-5">
                    <div className="grid grid-cols-2 p-5 pt-8">
                        <div>
                            <h3 className="font-semibold text-lg capitalize">
                                Manajemen Data Users
                            </h3>
                            <h4 className="font-medium  text-gray-600">
                                Tabel Users
                            </h4>
                        </div>
                        <div className="text-right">
                            <InertiaLink href={route("users.create")}>
                                <button className="button">
                                    <i className="fa fa-plus"></i> Tambah Users
                                </button>
                            </InertiaLink>
                        </div>
                    </div>
                    <div className="table-responsive mt-5 pt-2">
                        <div className="px-5">
                            <FlashMessages />
                            <FilterDatatable></FilterDatatable>
                        </div>
                        <table id="add-row" className="w-full border-b">
                            <thead>
                                <DatatableHeader
                                    headerField={headerField}
                                    order_field={order_field}
                                    order_mode={order_mode}
                                ></DatatableHeader>
                            </thead>
                            <tbody>
                                {data.data.length > 0 ? (
                                    data.data.map((items, index) => (
                                        <tr key={index}>
                                            <td className="p-3">
                                                {(data.current_page - 1) *
                                                    data.per_page +
                                                    index +
                                                    1}
                                            </td>
                                            <td className="p-3">
                                                {items.name}
                                            </td>
                                            <td className="p-3">
                                                {items.email}
                                            </td>
                                            <td className="p-3">
                                                {items.role_name != null &&
                                                    items.role_name
                                                        .split(";;")
                                                        .map(
                                                            (
                                                                items_role,
                                                                index_role
                                                            ) => (
                                                                <span
                                                                    className="badge badge-success mr-1"
                                                                    key={
                                                                        index_role
                                                                    }
                                                                >
                                                                    {items_role}
                                                                </span>
                                                            )
                                                        )}
                                            </td>
                                            <td className="p-3">
                                                <Action
                                                    module="users"
                                                    detail_url={route(
                                                        "users.show",
                                                        items.id_user
                                                    )}
                                                    delete_url={route(
                                                        "users.destroy",
                                                        items.id_user
                                                    ).toString()}
                                                    edit_url={route(
                                                        "users.edit",
                                                        items.id_user
                                                    )}
                                                    handleShowDeleteModal={
                                                        handleShowDeleteModal
                                                    }
                                                ></Action>
                                            </td>
                                        </tr>
                                    ))
                                ) : (
                                    <tr>
                                        <td
                                            colSpan={"100%"}
                                            className="text-center h-96"
                                        >
                                            <div className="flex items-center justify-center h-96">
                                                <div className="space-y-10">
                                                    <i className="fa fa-sad-tear fa-3x"></i>
                                                    <br></br>
                                                    Can't find any Data
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <Pagination
                                            data={data.links}
                                            from={data.from}
                                            to={data.to}
                                            total={data.total}
                                        ></Pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <DeleteModal
                show={deleteModal.show}
                handleCloseDeleteModal={handleCloseDeleteModal}
                handleDeleteAction={handleDeleteAction}
            ></DeleteModal>
        </React.Fragment>
    );
};

Index.layout = (page) => <Layout children={page} title="Users" />;
export default Index;
