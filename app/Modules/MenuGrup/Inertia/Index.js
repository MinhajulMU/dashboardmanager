import React, { useState } from "react";
import Layout from "@/Pages/Layout";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
import FlashMessages from "@/Shared/components/FlashMessages";
import DatatableHeader from "@/Shared/components/DatatableHeader";
import Pagination from "@/Shared/components/Pagination";
import FilterDatatable from "@/Shared/components/FilterDatatable";
import Action from "@/Shared/components/Action/Action";
import DeleteModal from "@/Shared/components/DeleteModal";
import { Inertia } from "@inertiajs/inertia";
import Sadrow from "@/Shared/components/Sadrow";

const Index = () => {
    const props = usePage().props;
    const { headerField, data, order_field, order_mode } = usePage().props;
    const [deleteModal, setDeleteModal] = useState({
        show: false,
        url: "",
    });

    const handleCloseDeleteModal = () =>
        setDeleteModal({ ...deleteModal, show: false });
    const handleShowDeleteModal = (deleteUrl) => {
        setDeleteModal({
            url: deleteUrl,
            show: true,
        });
    };
    const handleDeleteAction = () => {
        Inertia.delete(deleteModal.url);
        setDeleteModal({
            show: false,
        });
    };

    return (
        <React.Fragment>
            <div className="container mx-auto min-h-screen">
                <div className="md:-mb-3 space-y-2 mt-16">
                    <div className="grid grid-cols-2">
                        <div>
                            <h1 className="font-semibold md:text-2xl text-lg">
                                {" "}
                                Menu Grup
                            </h1>
                        </div>
                        <div className="text-right"></div>
                    </div>
                </div>
                <div className="card mt-5">
                    <div className="grid grid-cols-2 p-5 pt-8">
                        <div>
                            <h3 className="font-semibold text-lg capitalize">
                                Manajemen Data Menu Grup
                            </h3>
                            <h4 className="font-medium  text-gray-600">
                                Tabel Menu Grup
                            </h4>
                        </div>
                        <div className="text-right">
                            <InertiaLink href={route("menu-grup.create")}>
                                <button className="button ml-auto">
                                    <i className="fa fa-plus"></i> Tambah Menu
                                    Grup
                                </button>
                            </InertiaLink>
                        </div>
                    </div>
                    <div className="table-responsive mt-5 pt-2">
                        <div className="px-5">
                            <FlashMessages />
                            <FilterDatatable></FilterDatatable>
                        </div>
                        <table id="add-row" className="w-full border-b">
                            <thead>
                                <DatatableHeader
                                    headerField={headerField}
                                    order_field={order_field}
                                    order_mode={order_mode}
                                ></DatatableHeader>
                            </thead>
                            <tbody>
                                {data.data.length > 0 ? (
                                    data.data.map((items, index) => (
                                        <tr key={index}>
                                            <td className="p-3">
                                                {(data.current_page - 1) *
                                                    data.per_page +
                                                    index +
                                                    1}
                                            </td>
                                            <td className="p-3">
                                                {items.nm_menu_grup}
                                            </td>
                                            <td className="p-3">
                                                <i className={items.icon}></i>
                                            </td>
                                            <td className="p-3">
                                                {items.urutan}
                                            </td>
                                            <td className="p-3">
                                                <Action
                                                    module="menu-grup"
                                                    detail_url={route(
                                                        "menu-grup.show",
                                                        items.id_menu_grup
                                                    )}
                                                    edit_url={route(
                                                        "menu-grup.edit",
                                                        items.id_menu_grup
                                                    )}
                                                    delete_url={route(
                                                        "menu-grup.destroy",
                                                        items.id_menu_grup
                                                    ).toString()}
                                                    handleShowDeleteModal={
                                                        handleShowDeleteModal
                                                    }
                                                ></Action>
                                            </td>
                                        </tr>
                                    ))
                                ) : (
                                    <Sadrow></Sadrow>
                                )}
                            </tbody>
                        </table>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <Pagination
                                            data={data.links}
                                            from={data.from}
                                            to={data.to}
                                            total={data.total}
                                        ></Pagination>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <DeleteModal
                show={deleteModal.show}
                handleCloseDeleteModal={handleCloseDeleteModal}
                handleDeleteAction={handleDeleteAction}
            ></DeleteModal>
        </React.Fragment>
    );
};

Index.layout = (page) => <Layout children={page} title="Menu Grup" />;
export default Index;
