import React, { useState } from "react";
import Layout from "@/Pages/Layout";
import { InertiaLink, usePage, useForm } from "@inertiajs/inertia-react";
import BackButton from "@/Shared/components/BackButton";

const Show = ({ data, routes }) => {
  return (
    <React.Fragment>
      <div className="container mx-auto min-h-screen">
        <div className="md:-mb-3 space-y-2 mt-16">
          <div className="grid grid-cols-2">
            <div>
              <h1 className="font-semibold md:text-2xl text-lg">
                {" "}
                Detail Menu Grup
              </h1>
            </div>
            <div className="text-right">
              <BackButton>{routes.backUrl}</BackButton>
            </div>
          </div>
        </div>
        <div className="card mt-5">
          <div className="grid grid-cols-2 p-5 pt-8">
            <div>
              <h3 className="font-semibold text-lg capitalize">
                Detail Rincian Data
              </h3>
            </div>
            <div className="text-right"></div>
          </div>
          <div className="container">
            <table>
              <tbody>
                <tr>
                  <td className="px-4 py-2 align-top">Nama Menu Grup</td>
                  <td className="px-4 py-2 align-top" width="10px">
                    :
                  </td>
                  <td className="px-4 py-2">{data.nm_menu_grup}</td>
                </tr>

                <tr>
                  <td className="px-4 py-2 align-top">Icon</td>
                  <td className="px-4 py-2 align-top" width="10px">
                    :
                  </td>
                  <td className="px-4 py-2">{data.icon}</td>
                </tr>

                <tr>
                  <td className="px-4 py-2 align-top">Urutan</td>
                  <td className="px-4 py-2 align-top" width="10px">
                    :
                  </td>
                  <td className="px-4 py-2">{data.urutan}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

Show.layout = (page) => <Layout children={page} title="Detail Menu Grup" />;
export default Show;
