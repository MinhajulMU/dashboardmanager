<?php

namespace App\Modules\Dashboard\Controllers;

use ReflectionClass;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Modules\Dashboard\Controllers\MatchOldPassword;
use App\Modules\PesertaDetail\Models\PesertaDetail;
use Illuminate\Support\Facades\URL;
use App\Bardiz12\Auth\GenerateMenuUser;
use App\Bardiz12\Auth\CachePrivileges;
use App\Modules\UserRole\Models\UserRole;
use App\Modules\Users\Models\Users;
use App\Modules\Dokumen\Models\Dokumen;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Session;
use Storage;
use Hash;

class DashboardController extends Controller
{
    //
    protected $title = 'Dashboard';

    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $data = [];
        return Inertia::render('Dashboard::Index', $data);
    }

    public function changeRole($id_role)
    {
        $role = Auth::user()->roles()->where('zeta_role.id_role', $id_role)->first();
        if ($role) {
            session()->put('active_role', json_decode(json_encode($role), true));
            $menuGenerator = new GenerateMenuUser($role);
            $menu = $menuGenerator->generate();
            session()->put('menu', $menu);
            $cachePrivileges = new CachePrivileges($role);
            $privileges = $cachePrivileges->cache();
            session()->put('role_privileges', $privileges);
            return redirect()->route('dashboard.index')->with('success', 'Anda mengaktifkan Role ' . $role->role_name);
        }
        return redirect()->route('dashboard.index')->with('errors', ['Error, Gagal mengganti role']);
    }

    public function editProfil(Request $request)
    {
        $id_user = Auth::user()->id_user;
        $data['role'] = UserRole::where('id_user', $id_user)
            ->leftJoin('zeta_role', 'zeta_role.id_role', '=', 'zeta_user_role.id_role')
            ->get(['zeta_role.role_name', 'zeta_user_role.id_role']);
        $data['user'] = Users::find($id_user);
        $id_jns_dokumen = '133174b3-eec3-42fd-b0b6-1286e086f079';
        $dokumen = Dokumen::where('id_jns_dokumen', $id_jns_dokumen)->where('id_model', $id_user)->orderBy('zeta_dokumen.created_at', 'desc')->first();
        $id_role = session()->get('active_role')['id_role'];
        $data['dokumen'] = $dokumen;
        return Inertia::render('Dashboard::Profil', $data);
    }

    public function updateProfil(Request $request, $id)
    {
        $formData = $request->validate([
            'name'       => 'required|string',
            'email'       => 'required|string|email'
        ]);
        $users = Users::findOrFail($id);
        $users->update($formData);

        // update profile photo
        $data['id_jns_dokumen'] = '133174b3-eec3-42fd-b0b6-1286e086f079';
        // profile photo
        $data['model'] = (new ReflectionClass($users))->getName();
        $data['id_model'] = $users->id_user;

        if ($request->file('photo') != null) {
            $file = $request->file('photo');
            $name = time() . $file->getClientOriginalName();
            $filePath = 'images/' . $name;
            $users->deleteDokumen($data['model'], $data['id_model'], $data['id_jns_dokumen']);
            $dok = $users->saveDokumen($request->file('photo'), $data, true);
            if ($dok[0] ==  true) {
                session()->put('profile_photo', '/storage/uploads/' . $dok[2]->file_path . $dok[2]->file_name);
            }
        }
        return redirect()
            ->route('dashboard.profile.index')
            ->with('success', 'Berhasil memperbarui data!');
    }

    public function editPassword()
    {
        $data = [];
        $active_role = session()->get('active_role')['id_role'];
        return Inertia::render('Dashboard::EditPassword', $data);
    }

    public function updatePassword(Request $request)
    {
        $formData = $request->validate([
            'password_lama' => ['required', new MatchOldPassword],
            'password_baru' => ['required', 'min:6', 'same:konfirmasi_password_baru'],
            'konfirmasi_password_baru' => ['required', 'min:6'],
        ]);
        $users = Users::find(Auth::user()->id_user);
        $raw_data = json_encode($users);
        $data['password'] = Hash::make($request->password_baru);
        $users->update($data);
        return redirect()
            ->route('dashboard.edit.password.index')
            ->with('success', 'Berhasil mengubah password!');
    }
}
