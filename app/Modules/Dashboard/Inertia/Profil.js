import React, { useState } from "react";
import Layout from "@/Pages/Layout";
import { InertiaLink, usePage, useForm } from "@inertiajs/inertia-react";
import FlashMessages from "@/Shared/components/FlashMessages";
import { Inertia } from "@inertiajs/inertia";
import TextInput from "@/Shared/Form/TextInput";
import FileInput from "@/Shared/Form/FileInput";
import SubmitButton from "@/Shared/Form/SubmitButton";
import Moment from "react-moment";
import ReactHtmlParser from "react-html-parser";

const Profil = () => {
  const props = usePage().props;
  const { data, setData, errors, post, processing } = useForm({
    name: props.user.name || "",
    email: props.user.email || "",
    photo: "",
    _method: "PUT",
  });

  function handleSubmit(e) {
    e.preventDefault();
    post(route("dashboard.profile-update.update", props.user.id_user));
  }
  return (
    <React.Fragment>
      <div className="container mt-10 min-h-screen">
        <div className="md:-mb-3 space-y-2 mt-16">
          <div className="grid grid-cols-2">
            <div>
              <h1 className="font-semibold md:text-2xl text-lg">
                Edit Profile
              </h1>
            </div>
            <div className="text-right"></div>
          </div>
          <FlashMessages />
        </div>
        {/* Basic information */}
        <div className="grid lg:grid-cols-3 gap-8 md:mt-12">
          <div>
            <div uk-sticky="offset:100;bottom:true;media:992">
              <h3 className="text-lg mb-2 font-semibold"> Basic</h3>
              <p> Informasi Dasar</p>
            </div>
          </div>
          <div className="bg-white rounded-md lg:shadow-md shadow col-span-2">
            <div className="grid grid-cols-1 gap-3 lg:p-6 p-4">
              <form onSubmit={handleSubmit}>
                <TextInput
                  type="text"
                  label="Nama Lengkap"
                  name="name"
                  errors={props.errors.name}
                  value={data.name}
                  placeholder="Masukkan nama lengkap"
                  onChange={(e) => setData("name", e.target.value)}
                />
                <TextInput
                  type="email"
                  label="Email Address"
                  name="email"
                  errors={props.errors.email}
                  value={data.email}
                  placeholder="Masukkan email address"
                  onChange={(e) => setData("email", e.target.value)}
                />
                <div>
                  <FileInput
                    className="w-full pb-5"
                    label="Photo"
                    name="photo"
                    accept="image/*"
                    errors={props.errors.photo}
                    value={data.photo}
                    onChange={(photo) => setData("photo", photo)}
                  />
                  {props.dokumen != null
                    ? ReactHtmlParser(
                        "<span class='badge badge-success mb-5'>" +
                          props.dokumen.file_name +
                          "</span>"
                      )
                    : ""}
                </div>

                <div className="form-group">
                  <SubmitButton
                    name="Simpan"
                    isLoading={processing}
                  ></SubmitButton>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

Profil.layout = (page) => <Layout children={page} title="Profile" />;
export default Profil;
