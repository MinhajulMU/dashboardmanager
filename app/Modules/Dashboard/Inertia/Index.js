import React from "react";
import Layout from "@/Pages/Layout";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
import FlashMessages from "@/Shared/components/FlashMessages";

const Index = () => {
  const props = usePage().props;
  let id_role = props.auth.active_role.id_role;
  return (
    <React.Fragment>
      <div className="container min-h-screen">
        {/*  breadcrumb */}
        <div className="md:-mb-3 space-y-2 mt-16">
          <h6 className="font-medium"> Welcome {props.auth.user.name}!</h6>
          <h1 className="font-semibold md:text-2xl text-lg"> Dashboard</h1>
        </div>
        <div className="card p-5 mt-5">
          <FlashMessages></FlashMessages>
          <p>
            Selamat Datang di {process.env.MIX_APP_NAME}. Anda login sebagai{" "}
            <b>{props.auth.active_role.role_name}.</b>
          </p>
        </div>
      </div>
    </React.Fragment>
  );
};

Index.layout = (page) => <Layout children={page} title="Dashboard" />;
export default Index;
