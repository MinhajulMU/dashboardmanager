import React, { useState } from "react";
import Layout from "@/Pages/Layout";
import { InertiaLink, usePage, useForm } from "@inertiajs/inertia-react";
import FlashMessages from "@/Shared/components/FlashMessages";
import SubmitButton from "@/Shared/Form/SubmitButton";
import { Inertia } from "@inertiajs/inertia";
import TextInput from "@/Shared/Form/TextInput";

const Index = () => {
  const props = usePage().props;
  const { data, setData, errors, post, processing } = useForm({
    password_lama: "",
    password_baru: "",
    konfirmasi_password_baru: "",
  });
  function handleSubmit(e) {
    e.preventDefault();
    post(route("dashboard.edit.password.update"));
  }
  return (
    <React.Fragment>
      <div className="container mx-auto min-h-screen">
        <div className="md:-mb-3 space-y-2 mt-16">
          <div className="grid grid-cols-2">
            <div>
              <h1 className="font-semibold md:text-2xl text-lg">
                Edit Password
              </h1>
            </div>
            <div className="text-right"></div>
          </div>
          <FlashMessages />
        </div>
        <div className="card mt-5">
          <div className="grid grid-cols-2">
            
            <div className="text-right"></div>
          </div>
          <div className="container">
          <form onSubmit={handleSubmit}>
            <TextInput
              type="password"
              label="Password Lama"
              name="password_lama"
              errors={props.errors.password_lama}
              value={data.password_lama}
              placeholder="Masukkan Password Lama"
              onChange={(e) => setData("password_lama", e.target.value)}
            />
            <TextInput
              type="password"
              label="Password Baru"
              name="password_baru"
              errors={props.errors.password_baru}
              value={data.password_baru}
              placeholder="Masukkan Password Baru"
              onChange={(e) => setData("password_baru", e.target.value)}
            />
            <TextInput
              type="password"
              label="Konfirmasi Password Baru"
              name="konfirmasi_password_baru"
              errors={props.errors.konfirmasi_password_baru}
              value={data.konfirmasi_password_baru}
              placeholder="Masukkan Konfirmasi Password Baru"
              onChange={(e) =>
                setData("konfirmasi_password_baru", e.target.value)
              }
            />
            <div className="form-group">
              <SubmitButton
                name="Ubah Password"
                isLoading={processing}
              ></SubmitButton>
            </div>
          </form>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

Index.layout = (page) => <Layout children={page} title="Dashboard" />;
export default Index;
