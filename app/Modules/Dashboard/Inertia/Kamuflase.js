import React, { useState } from "react";
import Layout from "@/Pages/Layout";
import { InertiaLink, usePage, useForm } from "@inertiajs/inertia-react";
import FlashMessages from "@/Shared/components/FlashMessages";
import Select2 from "@/Shared/Form/Select2";
import SubmitButton from "@/Shared/Form/SubmitButton";
import { Inertia } from "@inertiajs/inertia";

const Index = () => {
  const props = usePage().props;
  const { data, setData, errors, post, processing } = useForm({
    id_user: "",
  });
  function handleSubmit(e) {
    e.preventDefault();
    post(route("kamuflase.store"));
  }
  return (
    <React.Fragment>
      <div className="container mx-auto min-h-screen">
        <div className="md:-mb-3 space-y-2 mt-16">
          <div className="grid grid-cols-2 ">
            <div>
              <h1 className="font-semibold text-lg capitalize md:text-2xl text-lg">Kamuflase</h1>
            </div>
            <div className="text-right"></div>
          </div>
        </div>
        <div className="card mt-5">
          <div className="card-body">
            <FlashMessages></FlashMessages>
            <div className="numbers p-5">
              <form onSubmit={handleSubmit}>
                <Select2
                  label="Pilih pengguna yang akan anda kamuflase:"
                  name="id_user"
                  errors={props.errors.id_user}
                  data={props.ref_user}
                  selected={data.id_user}
                  onChange={(selectedOption) =>
                    setData("id_user", selectedOption.value)
                  }
                  menuPortalTarget={document.body} 
                  styles={{ menuPortal: base => ({ ...base, zIndex: 9999 }) }}
                ></Select2>
                <div className="pl-2">
                  <SubmitButton
                    name="Kamuflase"
                    isLoading={processing}
                  ></SubmitButton>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

Index.layout = (page) => <Layout children={page} title="Kamuflase" />;
export default Index;
