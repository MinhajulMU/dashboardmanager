<?php

use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$slug = Str::snake($module,"-");

Route::group(['namespace' => 'App\Modules\\'.$module.'\Controllers','middleware' => ['web','auth','module.privilege','verified']], function () use ($slug, $module) {
    Route::resource($slug, $module . 'Controller');
    Route::get($slug . "/change-role/{id_role?}", $module . "Controller@changeRole")->name($slug . ".change-role.update");
});

Route::group(['namespace' => 'App\Modules\\'.$module.'\Controllers', 'middleware' => ['web','auth','module.privilege']], function () use ($slug, $module) {
    Route::get("profile/edit", $module . "Controller@editProfil")->name("dashboard.profile.index");
    Route::put('profile/{id_user}',$module . "Controller@updateProfil")->name("dashboard.profile-update.update");
    Route::get("password/edit", $module . "Controller@editPassword")->name("dashboard.edit.password.index");
    Route::post("password/update", $module . "Controller@updatePassword")->name("dashboard.edit.password.update");
});