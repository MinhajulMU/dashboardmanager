<?php

namespace App\Bardiz12\Auth;

use App\Modules\Role\Models\Role;
use App\Modules\RolePrivilege\Models\RolePrivilege;
use DB;

class GenerateMenuUser
{

    private $role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }

    public function generate()
    {

        $menu = [];
        $menu_grup = RolePrivilege::select('zeta_module.id_menu_grup', 'nm_menu_grup')
            ->where('id_role', $this->role->id_role)
            ->join('zeta_module', 'zeta_module.id_module', '=', 'zeta_role_privilege.id_module')
            ->join('zeta_menu_grup', 'zeta_module.id_menu_grup', '=', 'zeta_menu_grup.id_menu_grup')
            ->where('zeta_module.is_show', 1)
            ->where('zeta_role_privilege.active', 1)
            ->whereNull('zeta_module.deleted_at')
            ->whereNull('zeta_role_privilege.deleted_at')
            ->groupBy('zeta_module.id_menu_grup')
            ->orderBy('zeta_menu_grup.urutan', 'asc')
            ->get();
        foreach ($menu_grup as $key => $value) {

            $menu['grup_menu'][$key] = [
                'id' => $value->id_menu_grup,
                'nm' => $value->nm_menu_grup,
                'menu_utama' => null
            ];
            $menu_utama = RolePrivilege::select('zeta_module.id_module', 'zeta_module.name', 'zeta_module.icon', 'zeta_module.slug', 'parent_id')
                ->where('id_role', $this->role->id_role)
                ->join('zeta_module', 'zeta_module.id_module', '=', 'zeta_role_privilege.id_module')
                ->where('zeta_module.is_show', 1)
                ->where('zeta_role_privilege.active', 1)
                ->where('zeta_module.parent_id', '0')
                ->where('zeta_module.id_menu_grup', $value->id_menu_grup)
                ->whereNull('zeta_module.deleted_at')
                ->whereNull('zeta_role_privilege.deleted_at')
                ->orderBy('zeta_module.urutan', 'asc')
                ->get();
            if (count($menu_utama) > 0) {
                foreach ($menu_utama as $key_menu => $value_menu) {
                    $menu['grup_menu'][$key]['menu_utama'][$key_menu] = [
                        'nm'   => $value_menu->name,
                        'icon' => $value_menu->icon,
                        'slug' => $value_menu->slug,
                        'submenu' => null
                    ];
                    $submenu = RolePrivilege::select('zeta_module.name', 'zeta_module.icon', 'zeta_module.slug', 'parent_id')
                        ->where('id_role', $this->role->id_role)
                        ->join('zeta_module', 'zeta_module.id_module', '=', 'zeta_role_privilege.id_module')
                        ->where('zeta_module.is_show', 1)
                        ->where('zeta_role_privilege.active', 1)
                        ->where('zeta_module.parent_id', $value_menu->id_module)
                        ->whereNull('zeta_module.deleted_at')
                        ->whereNull('zeta_role_privilege.deleted_at')
                        ->orderBy('zeta_module.urutan', 'asc')
                        ->get();

                    if (count($submenu) > 0) {

                        foreach ($submenu as $key_submenu => $value_submenu) {
                            $menu['grup_menu'][$key]['menu_utama'][$key_menu]['submenu'][$key_submenu] = [
                                'nm'   => $value_submenu->name,
                                'icon' => $value_submenu->icon,
                                'url'  => route($value_submenu->slug . '.index'),
                            ];
                        }
                        $menu['grup_menu'][$key]['menu_utama'][$key_menu]['url'] = '#';
                    } else {
                        $menu['grup_menu'][$key]['menu_utama'][$key_menu]['url'] = route($value_menu->slug . '.index');
                    }
                }
            }
        }
        return $menu;
    }
}
