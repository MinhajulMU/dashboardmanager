<?php

namespace App\Bardiz12\Auth;

class CachePrivileges
{
    protected $role;
    public function __construct($role)
    {
        $this->role = $role;
    }

    public function cache()
    {
        $modules = $this->role->modules;
        $privileges = [];
        foreach ($modules as $module) {
            $privileges[$module->slug] = [
                'active' => $module->pivot->active
            ];
        }
        return $privileges;
    }
}
