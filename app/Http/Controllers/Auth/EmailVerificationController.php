<?php
namespace App\Http\Controllers\Auth;

use Inertia\Inertia;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

class EmailVerificationController extends Controller
{
    
    public function show()
    {
        if (auth()->user()->hasVerifiedEmail()) {
            return redirect()->route('login')->with('success', 'Email has been verified!');
        }else{
            return Inertia::render('Auth/VerifyEmail');
        }

    }

    public function request()
    {
        auth()->user()->sendEmailVerificationNotification();
        return back()
            ->with('success', 'Verification link sent!');
    }
    public function verify(EmailVerificationRequest $request)
    {
        $request->fulfill();
        return redirect()->to('/dashboard')->with('success', 'Email has been verified!'); // <-- change this to whatever you want
    }
}