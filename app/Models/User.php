<?php

namespace App\Models;

use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;
use Laravel\Sanctum\HasApiTokens;
use App\Notifications\VerifyEmail;



class User extends Model implements AuthenticatableContract, AuthorizableContract, MustVerifyEmailContract
{
    use  HasApiTokens, SoftDeletes, Authenticatable, Authorizable, HasFactory, MustVerifyEmail, Notifiable;
    public $incrementing = false;
    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $table = 'zeta_users';
    public $primaryKey = "id_user";

    public function roles()
    {
        return $this->belongsToMany('App\Modules\Role\Models\Role', 'zeta_user_role', 'id_user', 'id_role')->wherePivot('deleted_at', null);
    }
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }
}
