<?php
return [
    'db_name' => env('DB_DATABASE'),
    'db_password' => env('DB_PASSWORD'),
    'db_username' => env('DB_USERNAME'),
    'db_host' => env('DB_HOST'),
    'app_name' => env('APP_NAME')
];
