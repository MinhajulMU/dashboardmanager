import React, { useState, useEffect } from 'react';
import { Inertia } from '@inertiajs/inertia';
import { usePage } from '@inertiajs/inertia-react';
import ReactHtmlParser from 'react-html-parser';
import { usePrevious } from 'react-use';

const DatatableHeader = () => {
  const { headerField, order_field, order_mode } = usePage().props;
  const urlSearchParams = new URLSearchParams(window.location.search);
  const params = Object.fromEntries(urlSearchParams.entries());
  delete params['order_field'];
  delete params['order_mode'];

  const [values, setValues] = useState({
    order_field: order_field || '',
    order_mode: order_mode || ''
  });

  const prevValues = usePrevious(values);
  useEffect(() => {
    const query = values;
    if (prevValues) {
      Inertia.get(
        route(route().current()),
        { ...query, ...params },
        {
          replace: true,
          preserveState: true
        }
      );
    }
  }, [values]);

  const changeOrderField = (event,field) => {
    if (order_field !== null) {
      if (order_field == field) {
        if (order_mode == 'asc') {
          setValues(values => ({
            ...values,
            order_mode: 'desc'
          }));
        } else {
          setValues(values => ({
            ...values,
            order_field: null
          }));
        }
      } else {
        setValues(values => ({
          ...values,
          order_field: field,
          order_mode: 'asc'
        }));
      }
    } else {
      setValues(values => ({
        ...values,
        order_field: field,
        order_mode: 'asc'
      }));
    }
    Inertia.get(
      route(route().current()),
      { ...values, ...params },
      {
        replace: true,
        preserveState: true
      }
    );
  };
  return (
    <React.Fragment>
      <tr className="border-b bg-gray-50 text-base text-black">
        {headerField.map(({ name, field, sortable }) => (
          <th key={name} className="font-medium p-3">
            <div className="flex">
              <div className="flex-1">{name}</div>
              <div
                className="flex-1 text-right text-gray-600 cursor-pointer"
                onClick={() => {
                  changeOrderField(this,field);
                }}
              >
                {sortable == true
                  ? ReactHtmlParser(
                      order_field == field
                        ? order_mode == 'asc'
                          ? ('<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={1} d="M3 4h13M3 8h9m-9 4h6m4 0l4-4m0 0l4 4m-4-4v12" /></svg>')
                          : '<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-gray-600" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="1" d="M3 4h13M3 8h9m-9 4h9m5-4v12m0 0l-4-4m4 4l4-4" /></svg>'
                        : '<span class="text-gray-400 sort fa fa-sort text-sm"></span>'
                    )
                  : ''}
              </div>
            </div>
          </th>
        ))}
      </tr>
    </React.Fragment>
  );
};
export default DatatableHeader;
