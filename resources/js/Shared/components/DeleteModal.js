import React, {Fragment } from "react";
import { Dialog, Transition } from "@headlessui/react";


const DeleteModal = ({ show,handleCloseDeleteModal,handleDeleteAction }) => {
  return (
    <React.Fragment>
      <Transition appear show={show} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-1000 -top-8"
          onClose={handleCloseDeleteModal}
        >
          <div className="min-h-screen px-4 text-center">
            <Dialog.Overlay className="fixed inset-0 bg-gray-900 opacity-60" />

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <div className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-2xl">
                <Dialog.Title
                  as="h3"
                  className="text-lg font-medium leading-6 text-gray-900"
                >
                  Konfirmasi
                </Dialog.Title>
                <div className="mt-2">
                  <p className="text-sm text-gray-500">
                    Apakah anda yakin akan menghapus data ini ?
                  </p>
                </div>

                <div className="mt-4 text-right">
                  <button
                    type="button"
                    className="button gray small mr-2"
                    onClick={handleCloseDeleteModal}
                  >
                    Cancel
                  </button>
                  <button
                    type="button"
                    className="button danger small"
                    onClick={handleDeleteAction}
                  >
                    Delete
                  </button>
                </div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </React.Fragment>
  );
};

export default DeleteModal;
