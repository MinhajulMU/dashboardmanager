import { InertiaLink } from '@inertiajs/inertia-react';

const BackButton = url => {
  return (
    <div>
      <InertiaLink href={url.children}>
        <button className="button gray btn-round">
          <span className="icon-material-outline-arrow-back mr-1 text-sm"></span> Kembali
        </button>
      </InertiaLink>
    </div>
  );
};

export default BackButton;
