function Footer() {
  return (
    <div>
      <footer className="footer">
        <div className="container-fluid">
          <div className="copyright ml-auto">
            Copyright &copy; 2023 MNHJ Code
            
          </div>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
