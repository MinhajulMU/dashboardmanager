import ReactDOM from "react-dom";
import React, { useEffect } from "react";

const ModalPortal = (props) => {
  const modalRoot = document.createElement("div");

  useEffect(() => {
    document.body.appendChild(modalRoot);
    return () => {
      document.body.removeChild(modalRoot);
    };
  });

  return ReactDOM.createPortal(props.children, modalRoot);
};

export default ModalPortal;
