import React from "react";

const Sadrow = () => {
  return (
    <React.Fragment>
      <tr>
        <td colSpan={"100%"} className="text-center h-96">
          <div className="flex items-center justify-center h-96">
            <div className="space-y-10">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="mx-auto h-10 w-10"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M10 18a8 8 0 100-16 8 8 0 000 16zM7 9a1 1 0 100-2 1 1 0 000 2zm7-1a1 1 0 11-2 0 1 1 0 012 0zm-7.536 5.879a1 1 0 001.415 0 3 3 0 014.242 0 1 1 0 001.415-1.415 5 5 0 00-7.072 0 1 1 0 000 1.415z"
                  clipRule="evenodd"
                />
              </svg>
              <br></br>
              Can't find any Data
            </div>
          </div>
        </td>
      </tr>
    </React.Fragment>
  );
};

export default Sadrow;
