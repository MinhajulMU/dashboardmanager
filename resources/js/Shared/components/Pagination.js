import React from "react";
import { InertiaLink } from "@inertiajs/inertia-react";
import ReactHtmlParser from "react-html-parser";

const Pagination = (props) => {
  return (
    <React.Fragment>
      {props.total > 0 ? (
        <div className="flex items-center justify-between sm:flex-row flex-col mt-2 p-6">
          <div>
            <p className="text-sm leading-5">
              Showing {props.from} to {props.to} of {props.total}
            </p>
          </div>
          <div>
            <div className="border divide-x flex font-medium items-center rounded-md text-gray-600">
              {props.data.map((items, index) => (
                <InertiaLink
                  className={
                    (items.active == true
                      ? "py-2 px-3 text-black bg-gray-200"
                      : "py-2 px-3 text-black hover:bg-gray-100") +
                    " " +
                    (items.url == null ? "disabled" : "")
                  }
                  href={
                    (items.url == null ? "#" : items.url) +
                    (items.active == true ? "#" : "")
                  }
                  key={index}
                  preserveScroll
                >
                  {ReactHtmlParser(items.label)}
                </InertiaLink>
              ))}
            </div>
          </div>
        </div>
      ) : (
        ""
      )}
    </React.Fragment>
  );
};

export default Pagination;
