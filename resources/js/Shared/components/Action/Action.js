import { InertiaLink } from "@inertiajs/inertia-react";
import React from "react";

const Action = ({
    module,
    detail_url,
    delete_url,
    edit_url,
    handleShowDeleteModal,
}) => {
    return (
        <React.Fragment>
            <div>
                <a
                    href="#"
                    className="hover:bg-gray-200 p-1.5 inline-block rounded-full text-black"
                    aria-expanded="false"
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        className="w-6"
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z"
                        />
                    </svg>
                </a>
                <div
                    className="bg-white w-44 shadow-lg border border-gray-100 p-2 mt-12 rounded-md text-gray-500 hidden text-left font-medium"
                    uk-drop="pos: bottom-right"
                >
                    <ul className="space-y-1">
                        <li>
                            <InertiaLink
                                href={detail_url}
                                className="px-3 py-2 rounded-md block hover:bg-gray-100 text-black"
                            >
                                {" "}
                                View{" "}
                            </InertiaLink>
                        </li>
                        <li>
                            <InertiaLink
                                href={edit_url}
                                className="px-3 py-2 rounded-md block hover:bg-gray-100 text-black"
                            >
                                {" "}
                                Edit{" "}
                            </InertiaLink>
                        </li>
                        <li>
                            <hr className="-mx-2 my-2 dark:border-gray-800" />
                        </li>
                        <li>
                            <button
                                className="px-3 py-2 rounded-md block text-red-500 hover:bg-red-100 hover:text-red-500 w-full text-left"
                                onClick={() => {
                                    handleShowDeleteModal(delete_url);
                                }}
                            >
                                {" "}
                                Delete{" "}
                            </button>
                        </li>
                    </ul>
                </div>
            </div>
        </React.Fragment>
    );
};

export default Action;
