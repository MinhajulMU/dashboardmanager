import React, { useState } from "react";
import { AsyncPaginate } from "react-select-async-paginate";
import axios from "axios";

const Select2Async = ({ label, name, className, errors = [], ...props }) => {
  const [value, setValue] = useState(null);
  const  loadOptions = async (search, loadedOptions, { page }) => {
    let comboUrl = route("combo.universitas").toString();
    const response = await axios.get(comboUrl + `?q=${search}&page=${page}`);
    const responseJSON = await response.data;
    return {
      options: responseJSON.result.data,
      hasMore: responseJSON.hasMore,
      additional: {
        page: responseJSON.hasMore ? page+1 : page 
      }
    };
  }

  return (
    <React.Fragment>
      <div className={`form-group ${errors.length ? "has-error" : ""}`}>
        {label && <label htmlFor={name}>{label}</label>}
        <AsyncPaginate
          value={value}
          loadOptions={loadOptions}
          onChange={setValue}
        />
        {errors && (
          <div className="form-text text-muted text-red-500">{errors}</div>
        )}
      </div>
    </React.Fragment>
  );
};

export default Select2Async;
