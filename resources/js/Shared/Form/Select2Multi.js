import React, { useState } from "react";
import Select from "react-select";

const Select2Multi = ({ label, name, data, selected, errors = [], ...props }) => {
  function search(nameKey, myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].value === nameKey) {
        return myArray[i];
      }
    }
  }
  if (typeof props.isMulti !== "undefined") {
    var selectedData = selected;
  } else {
    var selectedData = search(selected, data);
  }
  const customStyles = {
    container: (provided, state) => ({
      ...provided,
      padding: 0,
      margin: 0,
    }),
    control: (provided, state) => ({
      ...provided,
      padding: 0,
      margin: 0
    }),
    valueContainer: (provided, state) => ({
      ...provided,
      padding: 0,
      paddingLeft: 10,
      paddingRight: 10,
      margin: 0,
    }),
    input: (provided, state) => ({
      ...provided,
      padding: 0,
      margin: 0,
    }),
    menuPortal: (provided, state) => ({
      ...provided,
    }),
  };
  return (
    <React.Fragment>
      <div className={`mb-5 ${errors.length ? "has-error" : ""}`}>
        {label && (
          <label className="font-medium" htmlFor={name}>
            {label}
          </label>
        )}
        <Select
          options={data}
          defaultValue={selectedData}
          isClearable={true}
          isMulti
          styles={customStyles}
          {...props}
        />
        {errors && (
          <div className="form-text text-muted text-red-500">{errors}</div>
        )}
      </div>
    </React.Fragment>
  );
};
export default Select2Multi;
