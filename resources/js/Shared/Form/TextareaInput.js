import React from 'react';

export default ({label, name, className, errors = [], ...props }) => {
  return (
    <React.Fragment>
      <div className={`form-group mb-4 ${errors.length ? 'has-error' : ''}`}>
        {label && <label htmlFor={name} className="font-medium">{label}</label>}
        <textarea className={`with-border col-span-2 p-4 min-h-full resize-none ${className}`} {...props} ></textarea>
        {errors && (
          <div className="form-text text-muted text-red-500">{errors}</div>
        )}
      </div>
    </React.Fragment>
  );
};
