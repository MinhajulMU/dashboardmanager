import React from "react";
const SubmitButtonCyan = ({ name, isLoading, ...props }) => {
  return (
    <React.Fragment>
      <button
        disabled={isLoading}
        className="bg-cyan-600 p-2.5 mt-5 px-4 rounded-md text-center text-white"
        type="submit"
        {...props}
      >
        <div className="flex justify-center	">
          {isLoading && (
            <div className="flex justify-center items-center mr-1">
              <div className="animate-spin rounded-full" role="status">
                <i className="fa fa-circle-notch"></i>
              </div>
            </div>
          )}
          {name}
        </div>
      </button>
    </React.Fragment>
  );
};

export default SubmitButtonCyan;