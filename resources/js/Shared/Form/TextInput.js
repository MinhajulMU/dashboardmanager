import React from 'react';

export default ({
  type,
  label,
  name,
  className,
  errors = [],
  ...props
}) => {
  return (
    <React.Fragment>
      <div className={`form-group mb-4 ${errors.length ? 'has-error' : ''}`}>
        {label && <label htmlFor={name} className="font-medium">{label}</label>}
        <input
          type={type}
          id={name}
          name={name}
          className={`shadow-none with-border ${className}`}
          {...props}
        />
        {errors && (
          <div className="form-text text-muted text-red-500">{errors}</div>
        )}
      </div>
    </React.Fragment>
  );
};
