import React, { useState, useEffect } from "react";
import Flatpickr from "react-flatpickr";
import "flatpickr/dist/themes/dark.css";

export default ({ type, label, name, className, errors = [], ...props }) => {
  let options  = {};
  if (type == 'datetime') {
    options = {
      dateFormat: "Y-m-d H:i",
    };
  }else if(type == 'date'){
    options = {
      allowInput: true,
      dateFormat: "Y-m-d",
    };
  }else if(type == 'time'){
    options = {
      enableTime: true,
      noCalendar: true,
      dateFormat: "H:i",
      time_24hr: true
    };
  }else if(type == 'timesecond'){
    options = {
      enableTime: true,
      noCalendar: true,
      enableSeconds:true,
      dateFormat: "H:i:s",
      defaultHour	: 0,
      time_24hr: true
    };
  }else if(type == 'daterange'){
    options = {
      mode: "range",
      dateFormat: "Y-m-d"
    };
  }
  return (
    <React.Fragment>
      <div className={`form-group mb-4  ${errors.length ? "has-error" : ""}`}>
        {label && <label htmlFor={name} className="font-medium">{label}</label>}
        <Flatpickr data-enable-time={type=='datetime' || type=='timesecond' ? true:false} data-enable-seconds={type=='timesecond' ? true:false} options={options} {...props} className="shadow-none with-border" />
        {errors && (
          <div className="form-text text-muted text-red-500">{errors}</div>
        )}
      </div>
    </React.Fragment>
  );
};
