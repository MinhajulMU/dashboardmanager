import React from "react";
const SubmitButton = ({ name, isLoading, ...props }) => {
  return (
    <React.Fragment>
      <button
        disabled={isLoading}
        className="button text-xs"
        type="submit"
        {...props}
      >
        {isLoading && (
          <div className="flex justify-center items-center mr-1">
            <div className="animate-spin rounded-full" role="status">
              <i className="fa fa-circle-notch"></i>
            </div>
          </div>
        )}
        {name}
      </button>
    </React.Fragment>
  );
};

export default SubmitButton;
