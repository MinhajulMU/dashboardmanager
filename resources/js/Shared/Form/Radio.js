import React from "react";

const Radio = ({ label, name, errors, value, choises, ...props }) => {
  return (
    <React.Fragment>
      <div className={`mb-4 ${errors ? "has-error" : ""}`}>
        <label htmlFor="tes" className="font-medium">
          {label}
        </label>
        <br />
        <div className="flex space-x-5 mt-2">
          {choises.length > 0 &&
            choises.map((items, index) => (
              <span key={index}>
                <label className="form-radio-label">
                  <input
                    className="uk-radio mb-2"
                    type="radio"
                    name={name}
                    defaultValue={items.key}
                    defaultChecked={items.key === value && true}
                    {...props}
                  />
                  <span className="form-radio-sign">{items.value}</span>
                </label>
              </span>
            ))}
        </div>

        {errors && (
          <div className="form-text text-muted text-red-500">{errors}</div>
        )}
      </div>
    </React.Fragment>
  );
};

export default Radio;
