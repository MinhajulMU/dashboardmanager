import React from "react";
import { Editor } from "@tinymce/tinymce-react";

const Wysiwyg = ({ label, errors = [], ...props }) => {
  return (
    <React.Fragment>
      <div className={`form-group mb-4 ${errors.length ? "has-error" : ""}`}>
        {label && <label className="font-bold">{label}</label>}
        <Editor
          {...props}
          apiKey='mwcorh07u7ikmmz1tfss9egi3kg1hpjxu8x40fjqtubjh5pv'
          init={{
            path_absolute : "http://localhost:8000/",
            height: 500,
            selector: 'textarea.my-editor',
            relative_urls: false,
            plugins: [
              "advlist autolink lists link image charmap print preview hr anchor pagebreak",
              "searchreplace wordcount visualblocks visualchars code fullscreen",
              "insertdatetime media nonbreaking save table directionality",
              "emoticons template paste textpattern"
            ],
            menubar: 'file edit view insert format tools table tc help',
            toolbar: "undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | forecolor backcolor casechange permanentpen formatpainter removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media pageembed template link anchor codesample | a11ycheck ltr rtl | showcomments addcomment",
            file_picker_callback : function(callback, value, meta) {
              var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
              var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
        
              var cmsURL =  '/laravel-filemanager?editor=' + meta.fieldname;
              if (meta.filetype == 'image') {
                cmsURL = cmsURL + "&type=Images";
              } else {
                cmsURL = cmsURL + "&type=Files";
              }
        
              tinyMCE.activeEditor.windowManager.openUrl({
                url : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no",
                onMessage: (api, message) => {
                  callback(message.content);
                }
              });
            }
          }}
        />
        {errors && (
          <div className="form-text text-muted text-red-500">{errors}</div>
        )}
      </div>
    </React.Fragment>
  );
};

export default Wysiwyg;
