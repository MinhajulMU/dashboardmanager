import React from "react";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
const Role = () => {
  const { auth } = usePage().props;
  return (
    <React.Fragment>
      <a href="#" className="header_widgets text-black">
        <i className="icon-feather-tag text-xl"></i>
      </a>
      <div uk-drop="mode: click" className="header_dropdown">
        <div className="drop_headline">
          <h4>{auth.active_role.role_name} </h4>Aktif
        </div>
        {auth.roles.length > 0 && (
          <ul className="dropdown_scrollbar" data-simplebar>
            {auth.roles.map((items, index) => (
              <li key={index}>
                <InertiaLink href={(auth.active_role.id_role == items.id_role ? "#" : route('dashboard.change-role.update',items.id_role))}>
                  <i className="icon-material-outline-bookmark-border text-xl text-blue-500	 mr-3"></i>
                  <div className="drop_content">
                    <strong>{items.role_name} </strong>
                    <p>
                      {" "}
                      {items.id_role == auth.active_role.id_role
                        ? "Aktif"
                        : "Ubah ke " + items.role_name}
                    </p>
                  </div>
                </InertiaLink>
              </li>
            ))}
          </ul>
        )}
      </div>
    </React.Fragment>
  );
};

export default Role;
