import React from "react";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
const Profile = () => {
  const { auth, id_user_asli } = usePage().props;
  return (
    <React.Fragment>
      <a href="#" className="text-black	flex items-center space-x-2">
        <div className="h-8 w-8 rounded-full overflow-hidden ml-3">
          <img
            src={auth.profile_photo}
            className="h-full w-full inset-0 object-cover  "
            alt=""
          />
        </div>
        <span className="sm:inline hidden"> {auth.user.name} </span>
      </a>
      <div
        uk-drop="mode: click;offset:5"
        className="header_dropdown profile_dropdown"
      >
        <ul>
          
          <li>
            <a href="#" className="user">
              <div className="user_avatar">
                <div className="h-12 w-12 rounded-full overflow-hidden mr-3">
                  <img
                    src={auth.profile_photo}
                    className="h-full w-full inset-0 object-cover  "
                    alt=""
                  />
                </div>
              </div>
              <div className="user_name">
                <div> {auth.user.name}</div>
                <span> {auth.user.email} </span>
              </div>
            </a>
          </li>
          <li>
            <hr />
          </li>
          <li>
            <InertiaLink href={route("dashboard.profile.index")}>
              <i className="icon-material-outline-account-circle text-lg mr-1"></i>
              My Account
            </InertiaLink>
          </li>
          <li>
            <InertiaLink href={route("dashboard.edit.password.index")}>
              <i className="icon-feather-edit-3 mr-2"></i>
              Edit Password
            </InertiaLink>
          </li>
          {id_user_asli != null && (
            <li>
              <InertiaLink href={route("dashboard.back-kamuflase.update")}>
                <i className="icon-feather-arrow-left-circle mr-1"></i> Back
                From Kamuflase
              </InertiaLink>
            </li>
          )}
          <li>
            <hr />
          </li>

          <li>
            <InertiaLink
              as="button"
              href={route("logout")}
              className="w-full text-left p-2 pl-4"
              method="post"
            >
              <i className="icon-feather-log-out mr-2"></i>
              Log Out
            </InertiaLink>
          </li>
        </ul>
      </div>
    </React.Fragment>
  );
};

export default Profile;
