import React from "react";
import { InertiaLink } from "@inertiajs/inertia-react";
const HeaderFront = () => {
  return (
    <React.Fragment>
      <div className="bg-white py-4 shadow">
        <div className="max-w-6xl mx-auto">
          <div className="flex items-center justify-center">
            <InertiaLink href={route("home")}>
              {/* Zeta Manager */}
              <img src="/images/icon/logo2.jpg" alt="" className="w-40" />
            </InertiaLink>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default HeaderFront;
