import React from "react";
const Footer = () => {
  return (
    <React.Fragment>
      <div className="mt-0 mb-7 px-12 border-t pt-7 pb-5">
        <div className="flex flex-col items-center justify-between lg:flex-row max-w-6xl mx-auto lg:space-y-0 space-y-3">
          <p className="capitalize font-medium"> © copyright 2023 {process.env.MIX_APP_NAME}</p>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Footer;
