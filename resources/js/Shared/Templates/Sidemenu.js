import React, { useState } from "react";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
import Submenu from "@/Shared/Templates/Submenu";

const Sidemenu = () => {
  const { auth } = usePage().props;
  let pathname = window.location.pathname;
  pathname.indexOf(1);
  pathname.toLowerCase();
  pathname = pathname.split("/")[1];

  return (
    <div className="sidebar">
      <div className="sidebar_inner" data-simplebar>
        {auth.menu != null && (
          <div>
            {auth.menu.grup_menu.length > 0 &&
              auth.menu.grup_menu.map((items, index) => {
                return (
                  <React.Fragment key={index}>
                    <ul data-sub-title={items.nm}>
                      {items.menu_utama.length > 0 &&
                        items.menu_utama.map((items_menu, index_menu) => {
                          return items_menu.submenu == null ? (
                            <li
                              className={
                                items_menu.slug == pathname ? "active" : ""
                              }
                              key={index_menu}
                            >
                              <InertiaLink href={items_menu.url}>
                                <i className={items_menu.icon} />
                                <span>{items_menu.nm}</span>
                              </InertiaLink>
                            </li>
                          ) : (
                            <Submenu
                              key={index_menu}
                              items_menu={items_menu}
                            ></Submenu>
                          );
                        })}
                    </ul>
                  </React.Fragment>
                );
              })}
          </div>
        )}
      </div>
      <div
        className="side_overly"
        uk-toggle="target: #wrapper ; cls: is-collapse is-active"
      />
    </div>
  );
};

export default Sidemenu;
