import React, { useState } from "react";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";

const Submenu = ({ items_menu }) => {
  const [submenuOpen, setSubmenuOpen] = React.useState(false);
  return (
    <React.Fragment>
      <li className={submenuOpen == true ? "active-submenu" : ""}>
        <React.Fragment>
          <InertiaLink href="#" onClick={() => setSubmenuOpen(!submenuOpen)}>
            <i className={items_menu.icon} />
            <span>{items_menu.nm} </span>
          </InertiaLink>
          <ul>
            {items_menu.submenu.map((items_submenu, index_submenu) => {
              return (
                <li key={index_submenu}>
                  <InertiaLink href={items_submenu.url}>
                    {items_submenu.nm}
                  </InertiaLink>
                </li>
              );
            })}
          </ul>
        </React.Fragment>
      </li>
    </React.Fragment>
  );
};
export default Submenu;
