import React, { useState, useEffect } from "react";
import { Inertia } from "@inertiajs/inertia";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
import { useForm } from "@inertiajs/inertia-react";
import { Helmet } from "react-helmet";
import "../../../assets/css/icons.css";
import "../../../assets/css/uikit.css";
import "../../../assets/css/style.css";
import FlashMessages from "@/Shared/components/FlashMessages";

const ForgetPasswordLink = () => {
    const props = usePage().props;
    const { data, setData, errors, post, processing } = useForm({
        token: props.token,
        email: "",
        password: "",
        password_confirmation: "",
    });
    function handleSubmit(e) {
        e.preventDefault();
        post(route("reset.password.post"));
    }
    return (
        <React.Fragment>
            <Helmet>
                <title>Reset Password | {process.env.MIX_APP_NAME}</title>
                <meta charset="utf-8" />
                <link
                    rel="icon"
                    type="image/png"
                    href={process.env.MIX_APP_LOGO}
                ></link>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                />
            </Helmet>
            <div
                id="wrapper"
                className="flex flex-col justify-between h-screen pt-10"
            >
                {/* Content*/}
                <div>
                    <div className="lg:p-12 max-w-xl lg:my-0 my-12 mx-auto p-6 space-y-">
                        <form
                            onSubmit={handleSubmit}
                            className="lg:p-10 p-6 space-y-3 relative bg-white shadow-xl rounded-md"
                        >
                            <h1 className="lg:text-2xl text-xl font-semibold mb-6">
                                {" "}
                                Reset Password
                            </h1>
                            <FlashMessages></FlashMessages>
                            <div
                                className={`form-group ${
                                    errors.email ? "has-error" : ""
                                }`}
                            >
                                <label
                                    htmlFor="username"
                                    className="placeholder"
                                >
                                    <b>Email Address</b>
                                </label>
                                <input
                                    id="username"
                                    name="username"
                                    type="email"
                                    className="login-input"
                                    value={data.email}
                                    onChange={(e) =>
                                        setData("email", e.target.value)
                                    }
                                />

                                {errors.email && (
                                    <div className="form-text text-muted text-red-500">
                                        {errors.email}
                                    </div>
                                )}
                            </div>
                            <div
                                className={`form-group mt-2 ${
                                    errors.email ? "has-error" : ""
                                }`}
                            >
                                <label
                                    htmlFor="password"
                                    className="placeholder"
                                >
                                    <b>Password Baru</b>
                                </label>
                                <div className="position-relative">
                                    <input
                                        id="password"
                                        name="password"
                                        type="password"
                                        className="login-input"
                                        value={data.password}
                                        onChange={(e) =>
                                            setData("password", e.target.value)
                                        }
                                    />
                                </div>

                                {errors.password && (
                                    <div className="form-text text-muted  text-red-500">
                                        {errors.password}
                                    </div>
                                )}
                            </div>
                            <div
                                className={`form-group mt-2 ${
                                    errors.email ? "has-error" : ""
                                }`}
                            >
                                <label
                                    htmlFor="password"
                                    className="placeholder"
                                >
                                    <b>Konfirmasi Password Baru</b>
                                </label>
                                <div className="position-relative">
                                    <input
                                        id="password_confirmation"
                                        name="password_confirmation"
                                        type="password"
                                        className="login-input"
                                        value={data.password_confirmation}
                                        onChange={(e) =>
                                            setData(
                                                "password_confirmation",
                                                e.target.value
                                            )
                                        }
                                    />
                                </div>

                                {errors.password_confirmation && (
                                    <div className="form-text text-muted  text-red-500">
                                        {errors.password_confirmation}
                                    </div>
                                )}
                            </div>
                            <div className="form-action mb-3">
                                <button
                                    disabled={processing}
                                    type="submit"
                                    className="bg-green-600 font-semibold p-2.5 mt-5 rounded-md text-center text-white w-full"
                                >
                                    <div className="flex justify-center items-center mr-1">
                                        {processing ? (
                                            <div
                                                className="animate-spin rounded-full"
                                                role="status"
                                            >
                                                <i className="icon-feather-rotate-ccw"></i>
                                            </div>
                                        ) : (
                                            <i className="icon-feather-refresh-cw mr-2"></i>
                                        )}
                                        <span>Reset Password</span>
                                    </div>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default ForgetPasswordLink;
