import React, { useState, useEffect } from "react";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
import { useForm } from "@inertiajs/inertia-react";
import { Helmet } from "react-helmet";
import "../../../assets/css/uikit.css";
import "../../../assets/css/style.css";
import FlashMessages from "@/Shared/components/FlashMessages";
import HeaderFront from "@/Shared/Templates/HeaderFront";

const Login = () => {
  const { data, setData, errors, post, processing } = useForm({
    email: "",
    password: "",
    remember: false,
  });

  function handleSubmit(e) {
    e.preventDefault();
    post(route("login.attempt"));
  }
  return (
    <React.Fragment>
      <Helmet>
        <title>Login | {process.env.MIX_APP_NAME}</title>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href={process.env.MIX_APP_LOGO}></link>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Helmet>
      <div id="wrapper" className="flex flex-col h-screen">
        {/* Content*/}
        <HeaderFront></HeaderFront>
        <div>
          <div className="lg:p-12 max-w-xl lg:my-10 my-12 mx-auto p-6 lg:pt-0 space-y-">
            <form
              onSubmit={handleSubmit}
              className="lg:p-10  p-6 space-y-3 relative bg-white shadow-xl rounded-md"
              autoComplete="off"
            >
              <h1 className="lg:text-2xl text-xl font-semibold mb-6">
                {" "}
                Login to {process.env.MIX_APP_NAME}
              </h1>
              <FlashMessages></FlashMessages>
              <div className={`form-group ${errors.email ? "has-error" : ""}`}>
                <label className="mb-2" htmlFor="username">
                  {" "}
                  Email{" "}
                </label>
                <input
                  type="email"
                  id="username"
                  placeholder="Email"
                  className="login-input"
                  onChange={(e) => setData("email", e.target.value)}
                />
                {errors.email && (
                  <div className="form-text text-muted text-red-500">
                    {errors.email}
                  </div>
                )}
              </div>
              <div
                className={`form-group mt-2 ${errors.password ? "has-error" : ""
                  }`}
              >
                <label className="mb-2" htmlFor="password">
                  {" "}
                  Password{" "}
                </label>
                <input
                  type="password"
                  id="password"
                  placeholder="password"
                  className="login-input"
                  onChange={(e) => setData("password", e.target.value)}
                  autoComplete="off"
                />
                {errors.password && (
                  <div className="form-text text-muted text-red-500">
                    {errors.password}
                  </div>
                )}
              </div>
              <div className="flex justify-between">
                <div className=""></div>
                <InertiaLink
                  href={route("forget.password.get")}
                  className="link float-right text-green-600 hover:text-green-500"
                >
                  Lupa Password ?
                </InertiaLink>
              </div>

              <div>
                <button
                  disabled={processing}
                  type="submit"
                  className="bg-green-600 font-semibold p-2.5 mt-5 rounded-md text-center text-white w-full"
                >
                  <div className="flex justify-center	">
                    {processing && (
                      <div className="flex justify-center items-center mr-1">
                        <div
                          className="animate-spin rounded-full"
                          role="status"
                        >
                          <i className="icon-material-outline-autorenew"></i>
                        </div>
                      </div>
                    )}
                    Login
                  </div>
                </button>
              </div>
              {/* <div className="uk-heading-line uk-text-center py-5"><span> Or, connect with </span></div>
              <div className="flex items-center space-x-2 justify-center">
                <a href={route('google.login')} className="w-full">
                  <div className="w-full p-2 bg-[#EA4335] text-white font-bold rounded-full text-center">
                    <span className=" icon-brand-google text-white mr-1"></span> Google
                  </div>
                </a>
              </div> */}
            </form>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};
export default Login;
