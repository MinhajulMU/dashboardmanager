import React, { useEffect } from "react";
import { InertiaLink } from "@inertiajs/inertia-react";
import { useForm } from "@inertiajs/inertia-react";
import { Helmet } from "react-helmet";
import "../../../assets/css/icons.css";
import "../../../assets/css/style.css";
import FlashMessages from "@/Shared/components/FlashMessages";

const ForgotPassword = () => {
    const { data, setData, errors, post, processing } = useForm({
        email: "",
        password: "",
        remember: false,
    });

    function handleSubmit(e) {
        e.preventDefault();
        post(route("forget.password.post"));
    }
    return (
        <React.Fragment>
            <Helmet>
                <title>Reset Password | {process.env.MIX_APP_NAME}</title>
                <meta charset="utf-8" />
                <link
                    rel="icon"
                    type="image/png"
                    href={process.env.MIX_APP_LOGO}
                ></link>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1"
                />
            </Helmet>
            <div
                id="wrapper"
                className="flex flex-col justify-between h-screen pt-10"
            >
                {/* Content*/}
                <div>
                    <div className="lg:p-12 max-w-xl lg:my-0 my-12 mx-auto p-6 space-y-">
                        <form
                            onSubmit={handleSubmit}
                            className="lg:p-10 p-6 space-y-3 relative bg-white shadow-xl rounded-md"
                        >
                            <h1 className="lg:text-2xl text-xl font-semibold mb-6">
                                {" "}
                                Reset Password
                            </h1>
                            <FlashMessages></FlashMessages>
                            <div
                                className={`form-group ${
                                    errors.email ? "has-error" : ""
                                }`}
                            >
                                <label
                                    htmlFor="username"
                                    className="placeholder"
                                >
                                    <b>Alamat Email</b>
                                </label>
                                <input
                                    id="username"
                                    name="username"
                                    type="email"
                                    className="login-input"
                                    placeholder="Type your email address"
                                    value={data.email}
                                    onChange={(e) =>
                                        setData("email", e.target.value)
                                    }
                                />

                                {errors.email && (
                                    <div className="form-text text-muted text-red-500">
                                        {errors.email}
                                    </div>
                                )}
                            </div>
                            <div className="form-action mb-3">
                                <button
                                    disabled={processing}
                                    type="submit"
                                    className="bg-green-600 font-semibold p-2.5 mt-2 rounded-md text-center text-white w-full"
                                >
                                    <div className="flex justify-center items-center ">
                                        {processing ? (
                                            <div
                                                className="animate-spin rounded-full mr-1"
                                                role="status"
                                            >
                                                <i className="icon-feather-rotate-ccw"></i>
                                            </div>
                                        ) : (
                                            <i className="icon-feather-mail mr-1"></i>
                                        )}
                                        <span>Kirim Link Reset Password</span>
                                    </div>
                                </button>
                            </div>
                            <div>
                                <span className="msg mr-1">
                                    Sudah punya akun ?
                                </span>
                                <InertiaLink
                                    href={route("login")}
                                    className="link text-green-600 hover:text-green-500"
                                >
                                    Masuk
                                </InertiaLink>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default ForgotPassword;
