import React from "react";
import { InertiaLink, useForm } from "@inertiajs/inertia-react";

import { Helmet } from "react-helmet";
import "../../../assets/css/icons.css";
import "../../../assets/css/auth/verify.css";
import FlashMessages from "@/Shared/components/FlashMessages";

const VerifyEmail = () => {
  const { data, setData, errors, post, processing } = useForm();
  const d = new Date();
  let year = d.getFullYear();
  function handleSubmit(e) {
    e.preventDefault();
    post(route("verification.request"));
  }
  return (
    <React.Fragment>
      <Helmet>
        <title>Verify Email | {process.env.MIX_APP_NAME}</title>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href={process.env.MIX_APP_LOGO}></link>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Helmet>
      <table
        className="email-wrapper"
        width="100%"
        cellPadding={0}
        cellSpacing={0}
      >
        <tbody>
          <tr>
            <td align="center">
              <table
                className="email-content"
                width="100%"
                cellPadding={0}
                cellSpacing={0}
              >
                {/* Logo */}
                <tbody>
                  <tr>
                    <td className="email-masthead">
                      <a className="email-masthead_name">
                        {process.env.MIX_APP_NAME}
                      </a>
                    </td>
                  </tr>
                  {/* Email Body */}
                  <tr>
                    <td className="email-body" width="100%">
                      <table
                        className="email-body_inner"
                        align="center"
                        width={570}
                        cellPadding={0}
                        cellSpacing={0}
                      >
                        {/* Body content */}
                        <tbody>
                          <tr>
                            <td className="content-cell">
                              <FlashMessages></FlashMessages>
                              <h1>Verifikasi Alamat Email Anda</h1>
                              <br></br>
                              <p>
                                Harap verifikasi alamat email Anda dengan
                                mengeklik tautan dalam Email yang baru saja kami
                                kirimkan kepada Anda. Terima kasih!
                              </p>
                              {/* Action */}
                              <table
                                className="body-action"
                                align="center"
                                width="100%"
                                cellPadding={0}
                                cellSpacing={0}
                              >
                                <tbody>
                                  <tr>
                                    <td align="center">
                                      <div>
                                        <form onSubmit={handleSubmit}>
                                          <button
                                            disabled={processing}
                                            className="focus:outline-none text-white text-sm py-2.5 px-5 border-b-4 border-gray-600 rounded-md bg-gray-500 hover:bg-gray-400"
                                            type="submit"
                                          >
                                            <div className="flex">
                                              {processing && (
                                                <div className="flex justify-center items-center mr-1">
                                                  <div
                                                    className="animate-spin rounded-full"
                                                    role="status"
                                                  >
                                                    <i className="icon-feather-rotate-ccw"></i>
                                                  </div>
                                                </div>
                                              )}
                                              <span>
                                                Request Email Verifikasi
                                              </span>
                                            </div>
                                          </button>
                                        </form>
                                      </div>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <p>
                                Terimakasih,
                                <br />
                                {process.env.MIX_APP_NAME} Team
                              </p>
                              {/* Sub copy */}
                              <table className="body-sub">
                                <tbody>
                                  <tr>
                                    <td></td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <table
                        className="email-footer"
                        align="center"
                        width={570}
                        cellPadding={0}
                        cellSpacing={0}
                      >
                        <tbody>
                          <tr>
                            <td className="content-cell">
                              <p className="sub center">
                                {process.env.MIX_APP_NAME} {year}
                              </p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
};

export default VerifyEmail;
