import React from "react";
import { InertiaLink, usePage } from "@inertiajs/inertia-react";
import { Helmet } from "react-helmet";
import "../../assets/css/icons.css";
import "../../assets/css/uikit.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "../../assets/css/style.css";

import { $ } from "react-jquery-plugin";
require("../../assets/js/uikit.js");
require("../../assets/js/tippy.all.min.js");
require("../../assets/js/simplebar.js");
require("../../assets/js/bootstrap-select.min.js");

import Footer from "@/Shared/Templates/Footer";
import Sidemenu from "@/Shared/Templates/Sidemenu";
import Profile from "@/Shared/Templates/Profile";
import Role from "@/Shared/Templates/Role";

const Index = ({ title, children }) => {
  return (
    <React.Fragment>
      <Helmet>
        <title>{title + " | " + process.env.MIX_APP_NAME}</title>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href={process.env.MIX_APP_LOGO}></link>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Helmet>
      <div id="wrapper" className="is-verticle admin-panel">
        {/*  Header  */}
        <header>
          <div className="header_inner">
            <div className="left-side">
              {/* Logo */}
              <div id="logo">
                <a href="/">
                  <h2 className="font-bold text-white hidden lg:block logo_inverse">
                    {process.env.MIX_APP_NAME}
                  </h2>
                  <h2 className="font-bold text-dark block lg:hidden logo_mobile">
                    {process.env.MIX_APP_NAME}
                  </h2>
                </a>
              </div>
              {/* icon menu for mobile */}
              <div
                className="triger"
                uk-toggle="target: #wrapper ; cls: is-active"
              ></div>
            </div>
            <div className="right-side">
              {/* Header search box  */}
              <div className="header_search w-full"></div>
              <div>
                {/* role */}
                <Role></Role>
                {/* profile */}
                <Profile></Profile>
              </div>
            </div>
          </div>
        </header>
        {/* Main Contents */}
        <div className="main_content ">
          {children}
          {/* footer */}
          <Footer></Footer>
        </div>
        {/* sidebar */}
        <Sidemenu></Sidemenu>
      </div>
    </React.Fragment>
  );
};

export default Index;
