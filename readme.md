# Zeta Manager
Basic dashboard with single page application laravel.
## Requirements

- PHP 8.2
- Maria DB 10.3.9
- Laravel 10
- Inertia React.js
- nodejs 20.3.1

## Installation

Clone the repo locally:

```sh
git clone git@gitlab.com:MinhajulMU/dashboardmanager.git
cd dashboardmanager
```

Install PHP dependencies:

```sh
composer install
```

Install NPM dependencies:

```sh
npm install
```

Build assets:

```sh
npm run dev
```

Setup configuration:

```sh
mkdir public/js
cp .env.dev .env
```

Generate application key:

```sh
php artisan key:generate
```

Create a MySQl database. You can also use another database (SQLite, Postgres), simply update your configuration accordingly.


Restore database

```sh
mysql -u [user] -p dump-zmanager-202307231855.sql < [filename].sql
```
the database already includes the basic configuration of the application

Run artisan server:

```sh
php artisan serve
```

```sh
php artisan storage:link
```


You're ready to go! [Visit Relacode](http://127.0.0.1:8000/) in your browser, and login with:

- **Username:** admin@admin.com
- **Password:** 12345678

## Run Menu Generator

To run menu generator, run this in your terminal:

```
php artisan modules:make [table_name]
```

Command will generate basic crud code, crud uses privileged permissions for each user and primary key settings using uuid